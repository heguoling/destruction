/*
 * @Author       : HGL
 * @Date         : 2022-06-16 14:24:45
 * @LastEditTime : 2022-06-22 15:38:47
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastActor.cpp
 */
#include "BlastActor.h"
#include "BlastAsset.h"
#include "BlastManager.h"
#include "../Importer/MeshImporter.h"
BlastActor::BlastActor()
{
	m_ulRefCount=1;
}

BlastActor::~BlastActor()
{
}

ULONG BlastActor::AddRef()
{
	return InterlockedIncrement((long volatile*)&m_ulRefCount);
}

ULONG BlastActor::Release()
{
	ULONG ulNewRef = InterlockedDecrement((long volatile*)&m_ulRefCount);
	if (ulNewRef == 0)
	{
		UnInit();
		delete this;
	}
	return ulNewRef;
}

int BlastActor::Init()
{
	return 1;
}

int BlastActor::UnInit()
{
	return 1;
}


void BlastActor::FrustumCulling(const BoundingFrustum& frustumInWorld)
{
    //size_t sz = m_pDestructionModel->m_pDestructionMesh.size();
    //m_InFrustum = false;
    //m_pBlastAsset->m_SubModelInFrustum.resize(sz);
    //for (size_t i = 0; i < sz; ++i)
    //{
    //    BoundingOrientedBox box;
    //    BoundingOrientedBox::CreateFromBoundingBox(box, m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh[i]->m_pMeshData.m_BoundingBox);
    //    box.Transform(box, m_Transform.GetLocalToWorldMatrixXM());
    //    m_pBlastAsset->m_SubModelInFrustum[i] = frustumInWorld.Intersects(box);
    //    m_InFrustum = m_InFrustum || m_pBlastAsset->m_SubModelInFrustum[i];
    //}
}

void BlastActor::CubeCulling(const DirectX::BoundingOrientedBox& obbInWorld)
{
    //size_t sz = m_pDestructionModel->m_pDestructionMesh.size();
    //m_InFrustum = false;
    //m_SubModelInFrustum.resize(sz);
    //for (size_t i = 0; i < sz; ++i)
    //{
    //    BoundingOrientedBox box;
    //    BoundingOrientedBox::CreateFromBoundingBox(box, m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh[i]->m_pMeshData.m_BoundingBox);
    //    box.Transform(box, m_Transform.GetLocalToWorldMatrixXM());
    //    m_pBlastAsset->m_SubModelInFrustum[i] = obbInWorld.Intersects(box);
    //    m_InFrustum = m_InFrustum || m_pBlastAsset->m_SubModelInFrustum[i];
    //}
}

void BlastActor::CubeCulling(const DirectX::BoundingBox& aabbInWorld)
{
    //size_t sz = m_pDestructionModel->m_pDestructionMesh.size();
    //m_InFrustum = false;
    //m_pBlastAsset->m_SubModelInFrustum.resize(sz);
    //for (size_t i = 0; i < sz; ++i)
    //{
    //    BoundingBox box;
    //    m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh[i]->m_pMeshData.m_BoundingBox.Transform(box, m_Transform.GetLocalToWorldMatrixXM());
    //    m_pBlastAsset->m_SubModelInFrustum[i] = aabbInWorld.Intersects(box);
    //    m_InFrustum = m_InFrustum || m_pBlastAsset->m_SubModelInFrustum[i];
    //}
}

void BlastActor::SetModel(const Model* pModel)
{
    m_pDestructionModel=pModel->m_pMesh;
    for(auto it= m_pDestructionModel->m_pDestructionMesh.begin();it!= m_pDestructionModel->m_pDestructionMesh.end();it++)
        (*it)->materials = pModel->materials;
	m_pDestructionModel->m_pMeshData.m_BoundingBox=pModel->boundingbox;
}
