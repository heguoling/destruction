/*
 * @Author       : HGL
 * @Date         : 2022-06-16 14:23:34
 * @LastEditTime : 2022-06-22 14:45:41
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastAsset.h
 */
#ifndef BLAST_ASSET
#define BLAST_ASSET
#include "../src/KG3D_DestructionAsset.h"
#include "../src/KG3D_DestructionMesh.h"
using namespace KG3D_Destruction;
class BlastManager;
struct MeshData;
namespace Nv::Blast {
	class ExtPxAsset;
}
class BlastAsset:public DestructionAsset
{
public:
	BlastAsset();
	~BlastAsset();
public:
	virtual ULONG AddRef();
	virtual ULONG Release();

	virtual int Init(const char* pcszFilePath);
	virtual int UnInit();
	virtual int SetDestructionManager(DestructionManager* pBlastManager);

	virtual size_t getBlastAssetSize() const;
	virtual float getBondHealthMax()const;
	virtual float getSupportChunkHealthMax()const;

	ExtPxAsset* GetExtPxAsset();
	NvBlastExtDamageAccelerator* getAccelerator()const;
public:
	DestructionMesh* m_pDestructionModel;
	std::vector<bool> m_SubModelInFrustum;
private:
	int _ReadBlastAssetFromFile(const char* pcszFilePath);
	int _Init();

private:
	ULONG m_ulRefCount;
	BlastManager* m_pBlastManager;

	
	ExtPxAsset* m_pExtPxAsset;
	NvBlastExtDamageAccelerator* m_pDamageAccelerator;
};

#endif //BLAST_ASSET