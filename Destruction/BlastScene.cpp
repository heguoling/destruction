#include "BlastScene.h"
#include "BlastActor.h"
#include "Blaster.h"
#include "Application.h"

#include "BlastManager.h"
#include "Common/XUtil.h"
#include <NvBlastTkFramework.h>
#include <NvBlastTkGroup.h>
#include <NvBlastExtPxManager.h>

#include <NvBlastPxCallbacks.h>
#include <NvBlastExtPxTask.h>
#include <NvBlastExtPxActor.h>

#include <PxPhysicsAPI.h>
#include <extensions/PxDefaultAllocator.h>
#include <extensions/PxDefaultErrorCallback.h>

#define PVD_HOST "127.0.0.1"

static physx::PxJoint* createPxJointCallback(ExtPxActor* actor0, const physx::PxTransform& localFrame0, ExtPxActor* actor1,
	const physx::PxTransform& localFrame1, physx::PxPhysics& physics, TkJoint& joint)
{
	PxDistanceJoint* pxJoint = PxDistanceJointCreate(physics, actor0 ? &actor0->getPhysXActor() : nullptr, localFrame0,
		actor1 ? &actor1->getPhysXActor() : nullptr, localFrame1);
	pxJoint->setMaxDistance(1.0f);
	return pxJoint;
}

BlastScene::BlastScene() :
	m_ulRefCount(1),
	m_pTaskManager(nullptr),
	m_pTkGroup(nullptr),
	m_pExtPxManager(nullptr),
	m_pExtImpactDamageManager(nullptr),
	m_pEventCallback(nullptr),
	m_pExtGroupTaskManager(nullptr),
	m_bRigidBodyLimitEnabled(true),
	m_uRigidBodyLimit(40000),
	m_bImpactDamageEnabled(true),
	m_bImpactDamageUpdatePending(false),
	m_bImpactDamageToStressEnabled(false),
	m_fDT(1.0f / 60)
{
}

BlastScene::~BlastScene()
{
}

ULONG BlastScene::AddRef()
{
	return InterlockedIncrement((long volatile*)&m_ulRefCount);
}

ULONG BlastScene::Release()
{
	ULONG ulNewRef = InterlockedDecrement((long volatile*)&m_ulRefCount);
	if (ulNewRef == 0)
	{
		UnInit();
		delete this;
	}
	return ulNewRef;
}

int BlastScene::Init(PxScene* pPxScene)
{
	moduleName;
	int nResult = 1;

	return nResult;
}

int BlastScene::AddActor(BlastActor* newActor) {
	m_pBlastActors[newActor->actor_ID] = newActor;
	return 1;
}

BlastActor* BlastScene::GetActor(size_t id) {
	return m_pBlastActors[id];
}

int BlastScene::RemoveActor(size_t id) {
	m_pBlastActors.erase(id);
	return 1;
}

bool BlastScene::UpdateScene() {
	//m_pScene->simulate(1.0f / 60.0f);
	//m_pScene->fetchResults(true);
	return true;
}

int BlastScene::UnInit()
{
	return 1;
}

int BlastScene::FrameMove()
{
	int nResult = 0;

	m_pExtImpactDamageManager->applyDamage();

	nResult = 1;
	return nResult;
}

int BlastScene::_SetImpactDamageEnabled(bool bEnabled, bool bForceUpdate)
{
	int nResult = 0;

	if (m_bImpactDamageEnabled != bEnabled || bForceUpdate)
	{
		m_bImpactDamageEnabled = bEnabled;
		m_bImpactDamageUpdatePending = true;
	}

	nResult = 1;
	return nResult;
}
