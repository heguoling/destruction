/*
 * @Author       : HGL
 * @Date         : 2022-06-27 16:26:57
 * @LastEditTime : 2022-07-15 14:49:49
 * @Description  :
 * @FilePath     : \Destrution\Destruction\Common\Graphics.h
 */
#ifndef MESH_DATA_H
#define MESH_DATA_H

#include <wrl/client.h>
#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
#include <assert.h>
using namespace DirectX;

#define GRAPHICASSERT(X) \
    assert(X);

namespace Graphics
{
    /**
     * @brief: 点（坐标、颜色）
     *
     */
    struct VertexPC
    {
        XMFLOAT3 position;
        XMFLOAT4 color;
    };

    /**
     * @brief: 点（坐标、法线、纹理坐标）
     */
    struct VertexPNT
    {
        XMFLOAT3 position;
        XMFLOAT3 normal;
        XMFLOAT2 texcoord;
        // std::vector<XMFLOAT2>  texcoord;
    };

    /**
     * @brief: 面（顶点）
     */
    struct Facet
    {
        VertexPNT vertices[3];
        UINT userData;
        UINT materialId;
        UINT smoothingGroup;
    };

    /**
     * @brief: Mesh顶点、索引数据
     */
    struct MeshData
    {
        // 使用模板别名(C++11)简化类型名
        template <class T>
        using ComPtr = Microsoft::WRL::ComPtr<T>;
        ComPtr<ID3D11Buffer> m_pVertices;
        ComPtr<ID3D11Buffer> m_pIndices;
        uint32_t m_VertexCount = 0;
        uint32_t m_IndexCount = 0;
        uint32_t m_MaterialIndex = 0;

        DirectX::BoundingBox m_BoundingBox;
        bool m_InFrustum = true;
    };
}

#endif
