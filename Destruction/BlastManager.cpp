#include "BlastManager.h"
#include "BlastScene.h"
#include "BlastActor.h"
#include "BlastAsset.h"

#include "Application.h"
#include <NvBlastExtAuthoringMesh.h>

#include "PxPhysicsAPI.h"
#include "NvBlastTkFramework.h"
#include "NvBlastExtSerialization.h"
#include "NvBlastExtTkSerialization.h"
#include "NvBlastExtPxSerialization.h"
#include "NvBlastExtPxCollisionBuilder.h"
#include "NvBlastPxCallbacks.h"
#include "NvBlastExtAuthoringConvexMeshBuilder.h"
#include "NvBlastExtPxManager.h"
#include "NvBlastExtAuthoringBondGenerator.h"
#include <NvBlastExtAuthoring.h>

#include "../physics_module/PhysicsModule.h"
using namespace physx;
using namespace Nv::Blast;
using namespace KG3D_Destruction;
BlastManager *g_BlastManager;

extern "C" __declspec(dllexport) int GetBlastManager(KG3D_Destruction::IKG3D_DestructionManager **ppiRetBlastManager)
{
	int nResult = 0;
	DESTRUCTION_ASSERT(*ppiRetBlastManager == NULL);
	*ppiRetBlastManager = g_BlastManager;
	nResult = 1;
Exit0:
	return nResult;
}

BlastManager::BlastManager() : m_pTkFramework(nullptr),
							   m_pExtSerialization(nullptr),
							   m_pPxProfileZoneManager(nullptr),
							   m_pPhysxCPUDispatcher(nullptr),
							   m_pFractureTool(nullptr),
							   m_pVoronoiSitesGenerator(nullptr),
							   m_pRandomGenerator(nullptr),
							   m_pAuthoringResult(nullptr)
{
}

BlastManager::~BlastManager()
{
}

int BlastManager::Init()
{
	m_pPxFoundation = PhysicsModule::Get()->GetPxFoundation();
	m_pPxPhysics = PhysicsModule::Get()->GetPxPhysics();
	m_pPxCooking = PhysicsModule::Get()->GetPxCooking();

	int nResult = 0;
	m_pTkFramework = NvBlastTkFrameworkCreate();

	// m_replay = new BlastReplay();

	const unsigned uNumTreads = 4;
	m_pPhysxCPUDispatcher = PxDefaultCpuDispatcherCreate(uNumTreads);

	m_pExtSerialization = NvBlastExtSerializationCreate();
	if (m_pExtSerialization != nullptr)
	{
		NvBlastExtTkSerializerLoadSet(*m_pTkFramework, *m_pExtSerialization);
		NvBlastExtPxSerializerLoadSet(*m_pTkFramework, *m_pPxPhysics, *m_pPxCooking, *m_pExtSerialization);
	}

	PxTolerancesScale scale;

	m_pFractureTool = NvBlastExtAuthoringCreateFractureTool();

	m_pFractureTool->setInteriorMaterialId(1000);
	m_pRandomGenerator = new RandomGenerator();
	m_pFractureTool->setRemoveIslands(remove_islands);
	fracture_type = FractureType::VoronoiUniform;
	nResult = 1;
	return nResult;
}

int BlastManager::UnInit()
{
	int nResult = 0;
	m_pFractureTool->release();
	if (m_pVoronoiSitesGenerator)
	{
		m_pVoronoiSitesGenerator->release();
	}
	delete m_pRandomGenerator;
	m_pRandomGenerator = nullptr;
	// for (std::vector<DestructionMesh*>::iterator it = m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh.begin(); it != m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh.end(); it++)
	//{
	//	(*it)->blast_mesh->release();
	//	delete* it;
	//	*it = nullptr;
	//	it = m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh.erase(it);
	// }

	nResult = 1;
	return nResult;
}

void BlastManager::CreateBlastModel()
{
	if (!m_pSelectedActor)
	{
		Logger::Debug(moduleName, "未选中BlastActor");
		return;
	}
	else
	{
		DestructionMesh *model = m_pSelectedActor->GetModel();
		MaterialManager::Get().createMaterial("..\\Texture\\stone.dds"); //默认内部材质
		for (auto it = model->m_pDestructionMesh.begin(); it != model->m_pDestructionMesh.end(); it++)
			CreateBlastMesh(*it);
		Logger::Debug(moduleName, "创建BlastMesh成功!");
	}
}

void BlastManager::CreateBlastMesh(DestructionMesh *mesh)
{
	Nv::Blast::Mesh *blast_mesh = NvBlastExtAuthoringCreateMesh(mesh->vertices, mesh->normals, mesh->texture_coords,
																mesh->m_pMeshData.m_VertexCount, mesh->indices, mesh->m_pMeshData.m_IndexCount);
	SetCurrentMesh(blast_mesh);
	// current_selected_mesh = m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh[0];
	Nv::Blast::ExtPxCollisionBuilder *collision_builder = ExtPxManager::createCollisionBuilder(*m_pPxPhysics, *m_pPxCooking);
	Nv::Blast::BlastBondGenerator *bond_generator = NvBlastExtAuthoringCreateBondGenerator(collision_builder);
	Nv::Blast::ConvexDecompositionParams collisionParameter;
	collisionParameter.maximumNumberOfHulls = 1;
	collisionParameter.voxelGridResolution = 0;
	m_pAuthoringResult = NvBlastExtAuthoringProcessFracture(*m_pFractureTool, *bond_generator, *collision_builder, collisionParameter);
	bond_generator->release();
	collision_builder->release();

	Nv::Blast::Mesh *m = m_pFractureTool->createChunkMesh(0);
	mesh->blast_mesh = m;
	mesh->chunk_depth = m_pFractureTool->getChunkDepth(0);
	mesh->materials.push_back(MaterialManager::Get().GetMaterial("..\\Texture\\stone.dds"));
}

int BlastManager::CreateBlastActor(DestructionActor **ppiRetBlastActor)
{
	int nResult = 0;

	BlastActor *pBlastActor = new BlastActor();

	nResult = pBlastActor->Init();
	DESTRUCTION_ASSERT(nResult);

	*ppiRetBlastActor = pBlastActor;

	nResult = 1;
Exit0:
	return nResult;
}

int BlastManager::CreateBlastAsset(const char *pcszFilePath, DestructionAsset **ppiRetBlastAsset)
{
	int nResult = 0;

	BlastAsset *pBlastAsset = new BlastAsset();

	nResult = pBlastAsset->SetDestructionManager(this);
	DESTRUCTION_ASSERT(nResult);

	nResult = pBlastAsset->Init(pcszFilePath);
	DESTRUCTION_ASSERT(nResult);

	*ppiRetBlastAsset = pBlastAsset;

	nResult = 1;
Exit0:
	return nResult;
}

PxFoundation *BlastManager::GetPxFoundation()
{
	return m_pPxFoundation;
}

PxPhysics *BlastManager::GetPxPhysics()
{
	return m_pPxPhysics;
}

PxCpuDispatcher *BlastManager::GetPxCpuDispatcher()
{
	return m_pPhysxCPUDispatcher;
}

TkFramework *BlastManager::GetTkFramework()
{
	return m_pTkFramework;
}

ExtSerialization *BlastManager::GetExtSerialization()
{
	return m_pExtSerialization;
}

void BlastManager::UpdateMesh()
{
	Nv::Blast::ExtPxCollisionBuilder *collision_builder = ExtPxManager::createCollisionBuilder(*m_pPxPhysics, *m_pPxCooking);
	Nv::Blast::BlastBondGenerator *bond_generator = NvBlastExtAuthoringCreateBondGenerator(collision_builder);
	Nv::Blast::ConvexDecompositionParams collisionParameter;
	collisionParameter.maximumNumberOfHulls = 1;
	collisionParameter.voxelGridResolution = 0;
	m_pAuthoringResult = NvBlastExtAuthoringProcessFracture(*m_pFractureTool, *bond_generator, *collision_builder, collisionParameter);

	bond_generator->release();
	collision_builder->release();
	UINT chunkCount = m_pAuthoringResult->chunkCount;
	m_pSelectedActor->GetSelectedMesh()->is_root = true;
	m_pSelectedActor->GetSelectedMesh()->m_pDestructionMesh.resize(chunkCount - 2);
	for (int i = 1; i < chunkCount; i++)
	{
		DestructionMesh *mesh = new DestructionMesh();
		Nv::Blast::Mesh *m = m_pFractureTool->createChunkMesh(i);
		mesh->blast_mesh = m;
		mesh->chunk_depth = m_pFractureTool->getChunkDepth(i);
		mesh->chunk_id = i;

		float *pos = m_pAuthoringResult->chunkDescs[i].centroid;
		float x = pos[0];
		float y = pos[1];
		float z = pos[2];

		int triangle_count = m_pAuthoringResult->geometryOffset[i + 1] - m_pAuthoringResult->geometryOffset[i];
		mesh->m_pMeshData.m_VertexCount = triangle_count * 3;
		mesh->vertices = new NvcVec3[mesh->m_pMeshData.m_VertexCount];
		mesh->normals = new NvcVec3[mesh->m_pMeshData.m_VertexCount];
		mesh->texture_coords = new NvcVec2[mesh->m_pMeshData.m_VertexCount];
		mesh->m_pMeshData.m_IndexCount = triangle_count * 3;
		mesh->indices = new UINT[mesh->m_pMeshData.m_IndexCount];
		int vertex_index = 0;
		int indice_index = 0;
		int start_triangle = m_pAuthoringResult->geometryOffset[i];
		int end_triangle = m_pAuthoringResult->geometryOffset[i + 1];
		int inter_mat = m_pFractureTool->getInteriorMaterialId();
		std::vector<int> inter_mat_indices;
		//mesh->mesh_transform = m_pSelectedActor->GetSelectedMesh()->mesh_transform;

		for (int j = start_triangle; j < end_triangle; j++)
		{
			Nv::Blast::Triangle &tr = m_pAuthoringResult->geometry[j];
			for (int vi = 0; vi < 3; vi++, vertex_index++)
			{
				auto &vertex = (&tr.a)[vi];
				mesh->vertices[vertex_index] = vertex.p;
				mesh->normals[vertex_index] = vertex.n;
				mesh->texture_coords[vertex_index].x = vertex.uv->x;
				mesh->texture_coords[vertex_index].y = vertex.uv->y;
				mesh->indices[indice_index] = vertex_index;
				indice_index++;
				// if (tr.materialId != inter_mat)
				//{
				//	mesh->indices[indice_index] = vertex_index;
				//	indice_index++;
				// }
				// else
				//{
				//	inter_mat_indices.push_back(vertex_index);
				// }
			}
		}
		mesh->mesh_transform = new Transform(*m_pSelectedActor->GetSelectedMesh()->mesh_transform);
		mesh->mesh_transform->SetParentTransform(m_pSelectedActor->GetSelectedMesh()->mesh_transform);
		
		int tmp_indices = indice_index;
		// for (auto& index : inter_mat_indices)
		//{
		//	mesh->indices[indice_index] = index;
		//	indice_index++;
		// }
		mesh->materials = m_pSelectedActor->GetSelectedMesh()->materials;

		mesh->CreateVerticesFromData();

		mesh->materials = m_pSelectedActor->GetSelectedMesh()->materials;

		if (m_pSelectedActor->GetSelectedMesh())
		{
			mesh->chunk_depth += m_pSelectedActor->GetSelectedMesh()->chunk_depth;
			m_pSelectedActor->GetSelectedMesh()->m_pDestructionMesh.push_back(mesh);
			mesh->base_displacement = XMFLOAT3(x, y, z);
			mesh->mesh_transform->SetPosition(/*current_selected_mesh->mesh_transform->GetGlobalPosition()*/ mesh->base_displacement.x * explosion_amount,
											  mesh->base_displacement.y * explosion_amount, mesh->base_displacement.z * explosion_amount);
		}
		m_pSelectedActor->GetSelectedMesh()->m_pDestructionMesh[i - 1] = mesh;
	}
}

void BlastManager::SetCurrentMesh(Nv::Blast::Mesh *current)
{
	if (m_pSelectedActor->GetSelectedMesh() && m_pSelectedActor->GetSelectedMesh()->blast_mesh == current)
		return;
	m_pFractureTool->setSourceMesh(current);
}

void BlastManager::RemoveChildChunks(DestructionMesh *mesh)
{
	if (m_pFractureTool->deleteChunkSubhierarchy(mesh->chunk_id))
	{
		DeleteChilds(mesh);
		mesh->is_root = false;
	}
}

void BlastManager::ApplyFracture()
{
	if (m_pVoronoiSitesGenerator)
		m_pVoronoiSitesGenerator->release();
	m_pRandomGenerator->seed(0);
	m_pVoronoiSitesGenerator = NvBlastExtAuthoringCreateVoronoiSitesGenerator(m_pSelectedActor->GetSelectedMesh()->blast_mesh, m_pRandomGenerator);

	switch (fracture_type)
	{
	case FractureType::VoronoiUniform:
		ApplyVoronoiFracture();
		break;
	case FractureType::VoronoiClustered:
		ApplyVoronoiClusteredFracture();
		break;
	case FractureType::VoronoiRadial:
		ApplyVoronoiRadialFracture();
		break;
	case FractureType::VoronoiInSphere:
		ApplyVoronoiInSphereFracture();
		break;
	case FractureType::VoronoiRemove:
		RemoveInSphereFracture();
		break;
	case FractureType::Slice:
		ApplySliceFracture();
		break;
	default:
		break;
	}
}

void BlastManager::ResetMesh()
{
	m_pFractureTool->reset();
	// for (auto it = m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh.begin();
	//	it != m_pBlastActor->m_pBlastAsset->m_pDestructionModel->m_pDestructionMesh.end(); it++) {
	//	DeleteChilds(*it);
	//	delete* it;
	//	*it = nullptr;
	// }
}

void BlastManager::OptimizeChunks()
{
	// m_pFractureTool->uniteChunks();
}

bool BlastManager::ApplyVoronoiFracture()
{
	Logger::Debug(moduleName, "Fracturing with Voronoi...");
	SetCurrentMesh(m_pSelectedActor->GetSelectedMesh()->blast_mesh);
	m_pVoronoiSitesGenerator->uniformlyGenerateSitesInMesh(cell_counts);
	const NvcVec3 *sites = nullptr;
	UINT sites_count = m_pVoronoiSitesGenerator->getVoronoiSites(sites);
	if (m_pFractureTool->voronoiFracturing(m_pSelectedActor->GetSelectedMesh()->chunk_id, sites_count, sites, replace_chunk) == 0)
	{
		UpdateMesh();
		Logger::Debug("Blaster", "破碎应用成功!");
		return true;
	}
	Logger::Error(moduleName, "Failed to fracture with Voronoi! %s", FUNCTION_PATH);
	return false;
}

bool BlastManager::ApplyVoronoiClusteredFracture()
{
	m_pVoronoiSitesGenerator->clusteredSitesGeneration(cluster_count, cell_counts, cluster_radius);
	const NvcVec3 *sites = nullptr;
	UINT sites_count = m_pVoronoiSitesGenerator->getVoronoiSites(sites);
	if (m_pFractureTool->voronoiFracturing(m_pSelectedActor->GetSelectedMesh()->chunk_id, sites_count, sites, replace_chunk) == 0)
	{
		UpdateMesh();
		return true;
	}
	return false;
}

bool BlastManager::ApplyVoronoiRadialFracture()
{
	NvcVec3 center = {radial_center.x, radial_center.y, radial_center.z};
	NvcVec3 normal = {radial_normal.x, radial_normal.y, radial_normal.z};
	m_pVoronoiSitesGenerator->radialPattern(center, normal, radial_radius, radial_angular_steps, radial_steps, radial_angle_offset, radial_variability);
	const NvcVec3 *sites = nullptr;
	UINT sites_count = m_pVoronoiSitesGenerator->getVoronoiSites(sites);
	if (m_pFractureTool->voronoiFracturing(m_pSelectedActor->GetSelectedMesh()->chunk_id, sites_count, sites, replace_chunk) == 0)
	{
		UpdateMesh();
		return true;
	}
	return false;
}

bool BlastManager::ApplyVoronoiInSphereFracture()
{
	NvcVec3 center = {sphere_center.x, sphere_center.y, sphere_center.z};
	m_pVoronoiSitesGenerator->generateInSphere(cell_counts, sphere_radius, center);
	const NvcVec3 *sites = nullptr;
	UINT sites_count = m_pVoronoiSitesGenerator->getVoronoiSites(sites);
	if (m_pFractureTool->voronoiFracturing(m_pSelectedActor->GetSelectedMesh()->chunk_id, sites_count, sites, replace_chunk) == 0)
	{
		UpdateMesh();
		return true;
	}
	return false;
}

bool BlastManager::RemoveInSphereFracture()
{
	NvcVec3 center = {sphere_center.x, sphere_center.y, sphere_center.z};
	m_pVoronoiSitesGenerator->deleteInSphere(sphere_radius, center, remove_sphere_eraser_probability);
	const NvcVec3 *sites = nullptr;
	UINT sites_count = m_pVoronoiSitesGenerator->getVoronoiSites(sites);
	if (m_pFractureTool->voronoiFracturing(m_pSelectedActor->GetSelectedMesh()->chunk_id, sites_count, sites, replace_chunk) == 0)
	{
		UpdateMesh();
		return true;
	}
	return false;
}

bool BlastManager::ApplySliceFracture()
{
	Nv::Blast::SlicingConfiguration config;
	config.angle_variations = slicing_angle_variation;
	// 	config.noiseAmplitude = slicing_noise_amplitude;
	// 	config.noiseFrequency = slicing_noise_frquency;
	// 	config.noiseOctaveNumber = slicing_noise_octave_number;
	config.offset_variations = slicing_offset_variations;
	/*	config.surfaceResolution = slicing_surface_resolution;*/
	config.x_slices = slicing_x_slices;
	config.y_slices = slicing_y_slices;
	config.z_slices = slicing_z_slices;

	if (m_pFractureTool->slicing(m_pSelectedActor->GetSelectedMesh()->chunk_id, config, replace_chunk, m_pRandomGenerator) == 0)
	{
		UpdateMesh();
		return true;
	}
	return false;
}

void BlastManager::DeleteChilds(DestructionMesh *mesh)
{
	for (std::vector<DestructionMesh *>::iterator it = mesh->m_pDestructionMesh.begin(); it != mesh->m_pDestructionMesh.end(); it = mesh->m_pDestructionMesh.erase(it))
	{
		DeleteChilds(*it);
		*it = nullptr;
	}
}

void BlastManager::SetFractureType(FractureType type)
{
	fracture_type = type;
}

BlastManager::FractureType BlastManager::GetFractureType() const
{
	return fracture_type;
}

void BlastManager::SetCellCounts(int counts)
{
	cell_counts = counts;
}

int BlastManager::GetCellCounts() const
{
	return cell_counts;
}

void BlastManager::SetReplaceChunk(bool replace)
{
	replace_chunk = replace;
}

bool BlastManager::GetReplaceChunk() const
{
	return replace_chunk;
}

void BlastManager::SetRemoveIslands(bool remove)
{
	remove_islands = remove;
}

bool BlastManager::GetRemoveIslands() const
{
	return remove_islands;
}

void BlastManager::SetClusterCount(int count)
{
	cluster_count = count;
}

int BlastManager::GetClusterCount() const
{
	return cluster_count;
}

void BlastManager::SetClusterRadius(float radius)
{
	cluster_radius = radius;
}

float BlastManager::GetClusterRadius() const
{
	return cluster_radius;
}

void BlastManager::SetRadialCenter(XMFLOAT3 center)
{
	radial_center = center;
}

XMFLOAT3 BlastManager::GetRadialCenter() const
{
	return radial_center;
}

void BlastManager::SetRadialNormal(XMFLOAT3 normal)
{
	radial_normal = normal;
}

XMFLOAT3 BlastManager::GetRadialNormal() const
{
	return radial_normal;
}

void BlastManager::SetRadialRadius(float radius)
{
	radial_radius = radius;
}

float BlastManager::GetRadialRadius() const
{
	return radial_radius;
}

void BlastManager::SetRadialAngularSteps(int steps)
{
	radial_angular_steps = steps;
}

int BlastManager::GetRadialAngularSteps() const
{
	return radial_angular_steps;
}

void BlastManager::SetRadialSteps(int steps)
{
	radial_steps = steps;
}

int BlastManager::GetRadialSteps() const
{
	return radial_steps;
}

void BlastManager::SetRadialAngleOffset(float offset)
{
	radial_angle_offset = offset;
}

float BlastManager::GetRadialAngleOffset() const
{
	return radial_angle_offset;
}

void BlastManager::SetRadialVariability(float variability)
{
	radial_variability = variability;
}

float BlastManager::GetRadialVariability() const
{
	return radial_variability;
}

void BlastManager::SetSphereCenter(XMFLOAT3 center)
{
	sphere_center = center;
}

XMFLOAT3 BlastManager::GetSphereCenter() const
{
	return sphere_center;
}

void BlastManager::SetSphereRadius(float radius)
{
	sphere_radius = radius;
}

float BlastManager::GetSphereRadius() const
{
	return sphere_radius;
}

void BlastManager::SetRemoveSphereCenter(XMFLOAT3 center)
{
	remove_sphere_center = center;
}

XMFLOAT3 BlastManager::GetRemoveSphereCenter() const
{
	return remove_sphere_center;
}

void BlastManager::SetRemoveSphereRadius(float radius)
{
	remove_sphere_radius = radius;
}

float BlastManager::GetRemoveSphereRadius() const
{
	return remove_sphere_radius;
}

void BlastManager::SetRemoveSphereEraserProbability(float probability)
{
	remove_sphere_eraser_probability = probability;
}

float BlastManager::GetRemoveSphereEraserProbability() const
{
	return remove_sphere_eraser_probability;
}

void BlastManager::SetSlicingAngleVariation(float variation)
{
	slicing_angle_variation = variation;
}

float BlastManager::GetSlicingAngleVariation() const
{
	return slicing_angle_variation;
}

void BlastManager::SetSlicingNoiseAmplitude(float amplitude)
{
	slicing_noise_amplitude = amplitude;
}

float BlastManager::GetSlicingNoiseAmplitude() const
{
	return slicing_noise_amplitude;
}

void BlastManager::SetSlicingNoiseFrequency(float frequency)
{
	slicing_noise_frquency = frequency;
}

float BlastManager::GetSlicingNoiseFrequency() const
{
	return slicing_noise_frquency;
}

void BlastManager::SetSlicingNoiseOctaveNumber(UINT number)
{
	slicing_noise_octave_number = number;
}

UINT BlastManager::GetSlicingNoiseOctaveNumber() const
{
	return slicing_noise_octave_number;
}

void BlastManager::SetSlicingOffsetVariations(float variations)
{
	slicing_offset_variations = variations;
}

float BlastManager::GetSlicingOffsetVariations() const
{
	return slicing_offset_variations;
}

void BlastManager::SetSlicingSurfaceResolution(UINT resolution)
{
	slicing_surface_resolution = resolution;
}

UINT BlastManager::GetSlicingSurfaceResolution() const
{
	return slicing_surface_resolution;
}

void BlastManager::SetSlicingX(int x)
{
	slicing_x_slices = x;
}

int BlastManager::GetSlicingX() const
{
	return slicing_x_slices;
}

void BlastManager::SetSlicingY(int y)
{
	slicing_y_slices = y;
}

int BlastManager::GetSlicingY() const
{
	return slicing_y_slices;
}

void BlastManager::SetSlicingZ(int z)
{
	slicing_z_slices = z;
}

int BlastManager::GetSlicingZ() const
{
	return slicing_z_slices;
}

int BlastManager::SetIneterMaterial(Material *material)
{

	if (m_pSelectedActor->GetSelectedMesh()->blast_mesh)
	{
		m_pSelectedActor->GetSelectedMesh()->materials.back() = material;
		return 1;
	}
	Logger::Error(moduleName, "失败!请先调用CreateBlastMesh! %s", FUNCTION_PATH);
	return 0;
}

// RandomGenerator
float RandomGenerator::getRandomValue()
{
	float r = static_cast<float>(rand());
	r = r / static_cast<float>(RAND_MAX);
	return r;
}

void RandomGenerator::seed(int32_t seed)
{
	srand(seed);
}
