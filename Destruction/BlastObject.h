/*
 * @Author       : HGL
 * @Date         : 2022-06-16 14:25:25
 * @LastEditTime : 2022-06-16 15:26:37
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastObject.h
 */
#ifndef BLAST_OBJECT
#define BLAST_OBJECT
#include "../src/KG3D_DestructionObject.h"

namespace Nv
{
	namespace Blast
	{
		class TkGroup;
		class TkFamily;
		class ExtPxFamily;
	}
}

class BlastScene;
class BlastManager;
class BlastAsset;

using namespace KG3D_Destruction;

class BlastObject:public DestructionObject
{
public:
	BlastObject();
	~BlastObject();

public:
	virtual ULONG AddRef();
	virtual ULONG Release();

	int Init(BlastScene* pBlastScene, KG3D_BlastObjectDesc& objectDesc);
	int UnInit();
	int SetBlastManager(BlastManager* pBlastManager);

private:
	ULONG m_ulRefCount;
	BlastManager* m_pBlastManager;
	BlastAsset* m_pBlastAsset;

	TkFamily* m_pTkFamily;
	ExtPxFamily* m_pExtPxFamily;
};

#endif //BLAST_OBJECT