#include "Blaster.h"
#include "BlastActor.h"
#include "BlastAsset.h"
#include "BlastManager.h"
#include "BlastScene.h"
#include "BlastErrorCallback.h"
#include "Common/XUtil.h"
#include "Application.h"
#include "../src/KG3D_DestructionMesh.h"

Blaster::Blaster(Application* applicaion):
	m_pBlastManager(new BlastManager()),
	m_pErrorCallback(new BlastErrorCallback())
{
	name = "Blaster";
	app = applicaion;
	m_pBlastScene = new BlastScene();
}
Blaster::~Blaster() {
	DESTRUCTION_DELETE(m_pBlastManager);
	DESTRUCTION_DELETE(m_pBlastScene);
	DESTRUCTION_DELETE(ground);
}

bool Blaster::Init() {
	if (!m_pBlastScene->Init())
		return false;
	Logger::Debug(name, "BlastScene初始化成功");

	if (!m_pBlastManager->Init())
		return false;
	Logger::Debug(name, "BlastManager初始化成功");

	NvBlastGlobalSetErrorCallback(m_pErrorCallback.get());

	InitResource();
	return true;
}

bool Blaster::InitResource() {
	BlastActor* object = new BlastActor();
	object->SetModel(app->m_pMeshImporter->CreateFromFile("..\\Model\\Rock\\RockFlatLong_Set.obj"));
	object->actor_ID = StringToID("..\\Model\\Rock\\RockFlatLong_Set.obj");
	object->GetModel()->mesh_id = object->actor_ID;
	object->GetModel()->is_root = true;
	object->GetTransform().SetScale(0.8f, 0.8f, 0.8f);
	app->m_pRenderer->AddRenderMesh(object->GetModel());
	m_pBlastScene->AddActor(object); 

	//app->blaster->m_pBlastScene->GetActor(StringToID("..\\Model\\Rock\\RockFlatLong_Set.obj"))->m_pBlastManager->CreateBlastMesh((float*)pAiMesh->mVertices, (UINT)pAiMesh->mNumVertices,
	//	(float*)pAiMesh->mNormals, (float*)uv,
	//	m_indices, (UINT)pAiMesh->mNumFaces * 3);

	Geometry::MeshData groundData = Geometry::CreatePlane(600, 600, 25, 25);
	ground = app->m_pMeshImporter->CreateFromGeometry("ground", groundData)->m_pMesh->m_pDestructionMesh[0];
	ground->materials.resize(1);
	ground->materials[0]= app->m_pMaterialManager->createMaterial("..\\Texture\\floor.dds");
	ground->mesh_transform->SetPosition(0, -10, 0);
	ground->m_pMeshData.m_MaterialIndex = 0;
	ground->name = "ground";
	BoundingBox::CreateFromPoints(ground->m_pMeshData.m_BoundingBox, 4,
		groundData.vertices.data(), sizeof(XMFLOAT3));
	ground->m_pMeshData.m_BoundingBox = ground->m_pMeshData.m_BoundingBox;
	app->m_pRenderer->AddRenderMesh(ground);
	return true;
}

bool Blaster::UpdateScene() {
	m_pBlastScene->UpdateScene();
	return true;
}

BlastManager* Blaster::GetBlastManager() {
	return m_pBlastManager;
}