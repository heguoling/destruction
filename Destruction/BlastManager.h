/**
 * @Author       : HGL
 * @Date         : 2022-06-16 14:05:28
 * @LastEditTime : 2022-07-15 13:19:33
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastManager.h
 */
#ifndef BLAST_MANAGER
#define BLAST_MANAGER
#include "../src/KG3D_DestructionManager.h"
#include <NvBlastExtAuthoringFractureTool.h>

#include "../src/KG3D_DestructionMesh.h"

class BlastActor;
class BlastAsset;
class BlastScene;


using namespace KG3D_Destruction;
namespace Nv
{
	namespace Blast
	{
		class TkFramework;
		class ExtSerialization;
		class FractureTool;
		class VoronoiSitesGenerator;
		class RandomGeneratorBase;
		struct AuthoringResult;
		class BlastBondGenerator;
		class ExtPxManager;
	}
}

class RandomGenerator : public RandomGeneratorBase
{
public:
	RandomGenerator() {
		remember = false;
	}
	float getRandomValue();
	void seed(int32_t seed);
private:
	bool remember;
};

class BlastManager:public DestructionManager 
{
public:
	BlastManager();
	virtual ~BlastManager();

public:
	virtual int Init();
	virtual int UnInit();

	virtual int CreateBlastActor(DestructionActor** ppiRetDestructionActor);

	/**
	 * @brief Create a Blast Asset object
	 * 
	 * @param pcszFilePath 
	 * @param ppiRetDestructionAsset 
	 * @return int 
	 */
	virtual int CreateBlastAsset(const char* pcszFilePath, DestructionAsset** ppiRetDestructionAsset);

	/**
	 * @brief Create a Blast Model object
	 * 
	 */
	virtual void CreateBlastModel();


	virtual void UpdateMesh();

	/**
	 * @brief Set the Current Mesh object
	 * 
	 * @param current 
	 */
	virtual void SetCurrentMesh(Nv::Blast::Mesh* current);

	/**
	 * @brief 
	 * 
	 * @param mesh 
	 */
	virtual void RemoveChildChunks(DestructionMesh* mesh);

	/**
	 * @brief 
	 * 
	 */
	virtual void ApplyFracture();
	virtual void ResetMesh();
	virtual void OptimizeChunks();

	virtual bool RemoveInSphereFracture();

	virtual void SetFractureType(FractureType type);
	virtual FractureType GetFractureType() const;
	virtual void SetCellCounts(int counts);
	virtual int GetCellCounts() const;
	virtual void SetReplaceChunk(bool replace);
	virtual bool GetReplaceChunk() const;
	virtual void SetRemoveIslands(bool remove);
	virtual bool GetRemoveIslands() const;

	virtual void SetClusterCount(int count);
	virtual int GetClusterCount() const;
	virtual void SetClusterRadius(float radius);
	virtual float GetClusterRadius() const;

	virtual void SetRadialCenter(XMFLOAT3 center);
	virtual XMFLOAT3 GetRadialCenter() const;
	virtual void SetRadialNormal(XMFLOAT3 normal);
	virtual XMFLOAT3 GetRadialNormal() const;
	virtual void SetRadialRadius(float radius);
	virtual float GetRadialRadius() const;
	virtual void SetRadialAngularSteps(int steps);
	virtual int GetRadialAngularSteps() const;
	virtual void SetRadialSteps(int steps);
	virtual int GetRadialSteps() const;
	virtual void SetRadialAngleOffset(float offset);
	virtual float GetRadialAngleOffset() const;
	virtual void SetRadialVariability(float variability);
	virtual float GetRadialVariability() const;

	virtual void SetSphereCenter(XMFLOAT3 center);
	virtual XMFLOAT3 GetSphereCenter() const;
	virtual void SetSphereRadius(float radius);
	virtual float GetSphereRadius() const;

	virtual void SetRemoveSphereCenter(XMFLOAT3 center);
	virtual XMFLOAT3 GetRemoveSphereCenter() const;
	virtual void SetRemoveSphereRadius(float radius);
	virtual float GetRemoveSphereRadius() const;
	virtual void SetRemoveSphereEraserProbability(float probability);
	virtual float GetRemoveSphereEraserProbability() const;

	virtual void SetSlicingAngleVariation(float variation);
	virtual float GetSlicingAngleVariation() const;
	virtual void SetSlicingNoiseAmplitude(float amplitude);
	virtual float GetSlicingNoiseAmplitude() const;
	virtual void SetSlicingNoiseFrequency(float frequency);
	virtual float GetSlicingNoiseFrequency() const;
	virtual void SetSlicingNoiseOctaveNumber(UINT number);
	virtual UINT GetSlicingNoiseOctaveNumber() const;
	virtual void SetSlicingOffsetVariations(float variations);
	virtual float GetSlicingOffsetVariations() const;
	virtual void SetSlicingSurfaceResolution(UINT resolution);
	virtual UINT GetSlicingSurfaceResolution() const;
	virtual void SetSlicingX(int x);
	virtual int GetSlicingX() const;
	virtual void SetSlicingY(int y);
	virtual int GetSlicingY() const;
	virtual void SetSlicingZ(int z);
	virtual int GetSlicingZ() const;

	virtual int SetIneterMaterial(Material* material);

	PxFoundation* GetPxFoundation();
	PxPhysics* GetPxPhysics();
	PxCpuDispatcher* GetPxCpuDispatcher();
	TkFramework* GetTkFramework();
	ExtSerialization* GetExtSerialization();

protected:
	virtual void DeleteChilds(DestructionMesh* mesh);
	virtual bool ApplyVoronoiFracture();
	virtual bool ApplyVoronoiClusteredFracture();
	virtual bool ApplyVoronoiRadialFracture();
	virtual bool ApplyVoronoiInSphereFracture();
	virtual bool ApplySliceFracture();

	virtual void CreateBlastMesh(DestructionMesh* mesh);
public:
	FractureTool* m_pFractureTool;
private:
	
	TkFramework* m_pTkFramework;
	
	VoronoiSitesGenerator* m_pVoronoiSitesGenerator;
	RandomGenerator* m_pRandomGenerator;
	AuthoringResult* m_pAuthoringResult;

	
	ExtSerialization* m_pExtSerialization;
	PxFoundation* m_pPxFoundation;
	PxPhysics* m_pPxPhysics;
	PxCooking* m_pPxCooking;
	PxProfileZoneManager* m_pPxProfileZoneManager;
	PxDefaultCpuDispatcher* m_pPhysxCPUDispatcher;

	//std::vector<DestructionMesh*> blast_meshes;

	Microsoft::WRL::ComPtr<ID3D11Device>m_pDevice;
	//Material* mesh_material;
	//Material* interior_mesh_material;

};

#endif //BLAST_MANAGER