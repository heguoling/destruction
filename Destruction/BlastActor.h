/*
 * @Author       : HGL
 * @Date         : 2022-06-16 14:24:33
 * @LastEditTime : 2022-06-22 15:38:20
 * @Description  :
 * @FilePath     : \Destrution\Destruction\BlastActor.h
 */
#ifndef BLAST_ACTOR
#define BLAST_ACTOR
#include "../src/KG3D_DestructionActor.h"
#include <windows.h>
#include "Common/Transform.h"
#include "Common/IEffect.h"
#include <DirectXCollision.h>
using namespace KG3D_Destruction;
class Transform;
class Material;
struct Model;
class BlastAsset;
class BlastManager;
namespace KG3D_Destruction {
	class DestructionManager;
}
class BlastActor : public DestructionActor
{
public:
	BlastActor();
	virtual ~BlastActor();

public:
	/**
	 * @brief 
	 * 
	 * @return ULONG 
	 */
	virtual ULONG AddRef();
	virtual ULONG Release();

	virtual int Init();
	virtual int UnInit();

	//
	// �ཻ���
	//
	virtual void FrustumCulling(const DirectX::BoundingFrustum &frustumInWorld);
	virtual void CubeCulling(const DirectX::BoundingOrientedBox &obbInWorld);
	virtual void CubeCulling(const DirectX::BoundingBox &aabbInWorld);



	Transform& GetTransform() { return m_Transform; };
	/**
	 * @brief Set the Model object
	 * 
	 * @param pModel 
	 */
	void SetModel(const Model *pModel);

public:
	//BlastManager* m_pBlastManager;
	//BlastAsset *m_pBlastAsset;
};

#endif // BLAST_ACTOR