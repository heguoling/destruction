/*
 * @Author       : HGL
 * @Date         : 2022-06-16 11:36:20
 * @LastEditTime : 2022-06-20 13:59:35
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastScene.h
 */
#ifndef BLAST_SCENE
#define BLAST_SCENE
#include "../src/KG3D_DestructionScene.h"
#include <PxPhysicsAPI.h>
#include <windows.h>
class BlastManager;
namespace KG3D_Destruction {
	class DestructionScene;
	class EventCallback;
}

namespace physx {
	class PxPhysics;
	class PxFoundation;
	class PxCooking;
	class PxDefaultErrorCallback;
	class PxDefaultAllocator;
}

using namespace KG3D_Destruction;

class BlastActor;
class BlastScene : public DestructionScene
{
public:
    BlastScene();
    virtual ~BlastScene();

public:
    virtual ULONG AddRef();
    virtual ULONG Release();

    virtual int Init(PxScene *pPxScene = 0);;
	
	int AddActor(BlastActor* newActor);
	BlastActor* GetActor(size_t id);
	int RemoveActor(size_t id);

	bool UpdateScene();

    virtual int UnInit();
    virtual int FrameMove();
private:
	int _SetImpactDamageEnabled(bool bEnabled, bool bForceUpdate = false);
private:
    ULONG m_ulRefCount;


	PxTaskManager* m_pTaskManager;
	TkGroup* m_pTkGroup;
	ExtPxManager* m_pExtPxManager;
	ExtImpactDamageManager* m_pExtImpactDamageManager;
	ImpactSettings m_pExtImpactDamageManagerSettings;
	EventCallback* m_pEventCallback;
	StressSolverSettings m_pExtStressSolverSettings;
	ExtGroupTaskManager* m_pExtGroupTaskManager;


	std::unordered_map<size_t, BlastActor*> m_pBlastActors;


	bool m_bRigidBodyLimitEnabled;
	unsigned m_uRigidBodyLimit;
	
	bool m_bImpactDamageEnabled;
	bool m_bImpactDamageUpdatePending;
	bool m_bImpactDamageToStressEnabled;

	float m_fDT;
};
#endif //BLAST_SCENE