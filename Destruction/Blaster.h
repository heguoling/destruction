#ifndef BLASTER
#define BLASTER
#include "Base.h"
#include "../Logger/Logger.h"
#include "BlastErrorCallback.h"
namespace {
	//Logger* logger = app->logger;
	std::string moduleName="Blaster";
}


class BlastActor;
class BlastScene;
class BlastManager;
class BlastAsset;

namespace KG3D_Destruction {
	class DestructionMesh;
}
class Blaster:public Base
{
public:
	Blaster(Application* application);
	~Blaster();
	bool Init();
	bool UpdateScene();
	bool InitResource();
	BlastManager* GetBlastManager();
public:
	BlastScene* m_pBlastScene;
	KG3D_Destruction::DestructionMesh* ground;
private:
	std::shared_ptr<BlastErrorCallback> m_pErrorCallback;
	BlastManager* m_pBlastManager;
};


#endif // BLASTER

