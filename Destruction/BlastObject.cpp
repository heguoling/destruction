/*
 * @Author       : HGL
 * @Date         : 2022-06-16 14:25:40
 * @LastEditTime : 2022-07-15 13:31:07
 * @Description  : 
 * @FilePath     : \Destrution\Destruction\BlastObject.cpp
 */
#include "BlastObject.h"
#include "BlastAsset.h"
#include "BlastManager.h"

#include "NvBlastTkFamily.h"
#include "NvBlastExtPxFamily.h"

#include "NvBlastExtPxManager.h"

#include<windows.h>

BlastObject::BlastObject() :
	m_ulRefCount(1),
	m_pBlastManager(nullptr),
	m_pBlastAsset(nullptr),
	m_pTkFamily(nullptr),
	m_pExtPxFamily(nullptr)
{
}

BlastObject::~BlastObject()
{
}

ULONG BlastObject::AddRef()
{
	return InterlockedIncrement((long volatile*)&m_ulRefCount);
}

ULONG BlastObject::Release()
{
	ULONG ulNewRef = InterlockedDecrement((long volatile*)&m_ulRefCount);
	if (ulNewRef == 0)
	{
		UnInit();
		delete this;
	}
	return ulNewRef;
}

int BlastObject::Init(BlastScene* pBlastScene, KG3D_BlastObjectDesc& objectDesc)
{
	int nResult = 0;
	
	//ExtPxFamilyDesc familyDesc;
	//familyDesc.actorDesc = nullptr; // if you use it one day, consider changing code which needs getBondHealthMax() from BlastAsset.
	//familyDesc.group = objectDesc.group;
	//familyDesc.pxAsset = m_pBlastAsset->GetExtPxAsset();
	//m_pExtPxFamily = m_pxManager.createFamily(familyDesc);
	//m_pExtPxFamily->setMaterial(&m_settings.material);


	//m_tkFamily = &m_pExtPxFamily->getTkFamily();
	//m_tkFamily->setID(objectDesc.id);

	//refreshDamageAcceleratorSettings();

	//m_familySize = NvBlastFamilyGetSize(m_tkFamily->getFamilyLL(), nullptr);

	//m_pxFamily->subscribe(m_listener);

	//m_initialTransform = objectDesc.transform;

	nResult = 1;
	return nResult;
}

int BlastObject::UnInit()
{
	return 1;
}

int BlastObject::SetBlastManager(BlastManager* pBlastManager)
{
	m_pBlastManager = pBlastManager;
	return 1;
}