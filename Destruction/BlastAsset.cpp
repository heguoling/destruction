#include "BlastAsset.h"
#include "BlastManager.h"

#include "NvBlastTkAsset.h"
#include "NvBlastExtPxAsset.h"
#include "NvBlastExtAssetUtils.h"
#include "NvBlastExtSerialization.h"
#include "NvBlastExtLlSerialization.h"
#include "NvBlastExtTkSerialization.h"
#include "NvBlastExtPxSerialization.h"
#include "NvBlastExtDamageShaders.h"

#include <fstream>
#include <vector>
#include <algorithm>
#include <windows.h>

BlastAsset::BlastAsset() :
	m_ulRefCount(1),
	m_pBlastManager(nullptr),
	m_pExtPxAsset(nullptr),
	m_pDamageAccelerator(nullptr)
{
	m_pDestructionModel = new DestructionMesh();
}

BlastAsset::~BlastAsset()
{
}

ULONG BlastAsset::AddRef()
{
	return InterlockedIncrement((long volatile*)&m_ulRefCount);
}

ULONG BlastAsset::Release()
{
	ULONG ulNewRef = InterlockedDecrement((long volatile*)&m_ulRefCount);
	if (ulNewRef == 0)
	{
		UnInit();
		delete this;
	}
	return ulNewRef;
}

int BlastAsset::Init(const char* pcszFilePath)
{
	int nResult = 0;
	int nRetCode = 0;
	
	nRetCode = _ReadBlastAssetFromFile(pcszFilePath);
	DESTRUCTION_ASSERT(nRetCode);


	
	nResult = 1;
Exit0:
	return nResult;
}

int BlastAsset::UnInit()
{
	if (m_pDamageAccelerator)
	{
		m_pDamageAccelerator->release();
	}
	return 1;
}

int BlastAsset::SetDestructionManager(DestructionManager* pBlastManager)
{
	m_pBlastManager = static_cast<BlastManager*>(pBlastManager);
	return 1;
}

ExtPxAsset* BlastAsset::GetExtPxAsset()
{
	return m_pExtPxAsset;
}

float BlastAsset::getBondHealthMax() const {
	return m_fBondHealthMax;
}

float BlastAsset::getSupportChunkHealthMax()const {
	return m_fSupportChunkHealthMax;
}

size_t BlastAsset::getBlastAssetSize() const
{
	return m_pExtPxAsset->getTkAsset().getDataSize();
}

NvBlastExtDamageAccelerator* BlastAsset::getAccelerator() const
{
	return m_pDamageAccelerator;
}

int BlastAsset::_ReadBlastAssetFromFile(const char* pcszFilePath)
{
	int nResult;

	const float unitConversion = 1.0f;
	const NvcVec3 inputScale = { unitConversion, unitConversion, unitConversion };
	
	std::ifstream stream(pcszFilePath, std::ios::binary);
	std::streampos size = stream.tellg();
	stream.seekg(0, std::ios::end);
	size = stream.tellg() - size;
	stream.seekg(0, std::ios::beg);
	std::vector<char> buffer(size);
	stream.read(buffer.data(), buffer.size());
	stream.close();

	uint32_t objectTypeID;
	void* asset = m_pBlastManager->GetExtSerialization()->deserializeFromBuffer(buffer.data(), buffer.size(), &objectTypeID);
	DESTRUCTION_ASSERT(asset);

	if (objectTypeID == Nv::Blast::ExtPxObjectTypeID::Asset)
	{
		m_pExtPxAsset = reinterpret_cast<ExtPxAsset*>(asset);
		const TkAsset& tkAsset = m_pExtPxAsset->getTkAsset();
		NvBlastAsset* llasset = const_cast<NvBlastAsset*>(tkAsset.getAssetLL());
		NvBlastExtAssetTransformInPlace(llasset, &inputScale, nullptr, nullptr);
		ExtPxSubchunk* subchunks = const_cast<ExtPxSubchunk*>(m_pExtPxAsset->getSubchunks());
		for (uint32_t i = 0; i < m_pExtPxAsset->getSubchunkCount(); ++i)
		{
			subchunks[i].geometry.scale.scale = PxVec3(unitConversion);
		}
	}
	else
	{
		DESTRUCTION_ASSERT(0); //目前只支持ExtPxAsset格式。
	}
	//else
	//{
	//	TkAsset* tkAsset = nullptr;
	//	if (objectTypeID == Nv::Blast::TkObjectTypeID::Asset)
	//	{
	//		tkAsset = reinterpret_cast<TkAsset*>(asset);
	//		NvBlastAsset* llasset = const_cast<NvBlastAsset*>(tkAsset->getAssetLL());
	//		NvBlastExtAssetTransformInPlace(llasset, &inputScale, nullptr, nullptr);
	//	}
	//	else if (objectTypeID == Nv::Blast::LlObjectTypeID::Asset)
	//	{
	//		NvBlastAsset* llasset = reinterpret_cast<NvBlastAsset*>(asset);
	//		NvBlastExtAssetTransformInPlace(llasset, &inputScale, nullptr, nullptr);
	//		tkAsset = m_pBlastManager->GetTkFramework()->createAsset(llasset, nullptr, 0, true);
	//	}
	//	else
	//	{
	//		BLAST_ASSERT(0);
	//	}

	//	if (tkAsset != nullptr)
	//	{
	//		std::vector<ExtPxAssetDesc::ChunkDesc> physicsChunks;
	//		std::vector<std::vector<ExtPxAssetDesc::SubchunkDesc> > physicsSubchunks;
	//		/**
	//		Try find FBX and check whether it contains collision geometry.
	//		*/
	//		objFileName.str("");
	//		objFileName << modelName << ".fbx";
	//		if (resourceManager.findFile(objFileName.str(), path))
	//		{
	//			std::shared_ptr<IFbxFileReader> rdr(NvBlastExtExporterCreateFbxFileReader(),
	//				[](IFbxFileReader* p) { p->release(); });
	//			rdr->loadFromFile(path.c_str());
	//			if (rdr->isCollisionLoaded() == 0)
	//			{
	//				ASSERT_PRINT(false, "fbx doesn't contain collision geometry");
	//			}
	//			uint32_t* hullsOffsets = nullptr;
	//			CollisionHull** hulls = nullptr;
	//			uint32_t meshCount = rdr->getCollision(hullsOffsets, hulls);

	//			physicsChunks.resize(meshCount);
	//			physicsSubchunks.resize(meshCount);

	//			std::shared_ptr<ExtPxCollisionBuilder> collisionBuilder(
	//				ExtPxManager::createCollisionBuilder(physics, cooking),
	//				[](Nv::Blast::ExtPxCollisionBuilder* cmb) { cmb->release(); });

	//			for (uint32_t i = 0; i < meshCount; ++i)
	//			{
	//				for (uint32_t sbHulls = hullsOffsets[i]; sbHulls < hullsOffsets[i + 1]; ++sbHulls)
	//				{
	//					PxConvexMeshGeometry temp =
	//						physx::PxConvexMeshGeometry(collisionBuilder.get()->buildConvexMesh(*hulls[sbHulls]));
	//					if (temp.isValid())
	//					{
	//						physicsSubchunks[i].push_back(ExtPxAssetDesc::SubchunkDesc());
	//						physicsSubchunks[i].back().geometry = temp;
	//						physicsSubchunks[i].back().transform = physx::PxTransform(physx::PxIdentity);
	//					}
	//				}
	//			}
	//			for (uint32_t i = 0; i < meshCount; ++i)
	//			{
	//				physicsChunks[i].isStatic = false;
	//				physicsChunks[i].subchunkCount = (uint32_t)physicsSubchunks[i].size();
	//				physicsChunks[i].subchunks = physicsSubchunks[i].data();
	//			}
	//			if (hulls && hullsOffsets)
	//			{
	//				for (uint32_t h = 0; h < hullsOffsets[meshCount]; h++)
	//				{
	//					collisionBuilder->releaseCollisionHull(hulls[h]);
	//				}
	//				NVBLAST_FREE(hulls);
	//				NVBLAST_FREE(hullsOffsets);
	//			}
	//		}
	//		m_pxAsset = ExtPxAsset::create(tkAsset, physicsChunks.data(), (uint32_t)physicsChunks.size());
	//		ASSERT_PRINT(m_pxAsset != nullptr, "can't create asset");
	//	}
	//}

	nResult = 1;
Exit0:
	return nResult;
}

int BlastAsset::_Init()
{
	int nResult;
	
	// calc max healths
	const auto& actorDesc = m_pExtPxAsset->getDefaultActorDesc();
	if (actorDesc.initialBondHealths)
	{
		m_fBondHealthMax = FLT_MIN;
		const uint32_t bondCount = m_pExtPxAsset->getTkAsset().getBondCount();
		for (uint32_t i = 0; i < bondCount; ++i)
		{
			m_fBondHealthMax = std::max<float>(m_fBondHealthMax, actorDesc.initialBondHealths[i]);
		}
	}
	else
	{
		m_fBondHealthMax = actorDesc.uniformInitialBondHealth;
	}

	if (actorDesc.initialSupportChunkHealths)
	{
		m_fSupportChunkHealthMax = FLT_MIN;
		const uint32_t nodeCount = m_pExtPxAsset->getTkAsset().getGraph().nodeCount;
		for (uint32_t i = 0; i < nodeCount; ++i)
		{
			m_fSupportChunkHealthMax = std::max<float>(m_fSupportChunkHealthMax, actorDesc.initialSupportChunkHealths[i]);
		}
	}
	else
	{
		m_fSupportChunkHealthMax = actorDesc.uniformInitialLowerSupportChunkHealth;
	}

	m_pDamageAccelerator = NvBlastExtDamageAcceleratorCreate(m_pExtPxAsset->getTkAsset().getAssetLL(), 3);

	nResult = 1;
	return nResult;
}
