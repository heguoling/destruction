#ifndef BLAST_ERROR_CALLBACK_H
#define BLAST_ERROR_CALLBACK_H

#include "NvBlastGlobals.h"
#include "NvBlastAssert.h"
#include "../Logger/Logger.h"
#include <assert.h>
#include <iostream>
#include <iosfwd>
#include <Windows.h>
namespace Nv::Blast {
	class ErrorCallback;
	struct ErrorCode;
}
class BlastErrorCallback : public Nv::Blast::ErrorCallback
{
	virtual void reportError(Nv::Blast::ErrorCode::Enum code, const char* msg, const char* file, int line) override {
		bool critical = false;
		switch (code)
		{
		case Nv::Blast::ErrorCode::Enum::eNO_ERROR:			
			Logger::Debug("NvBlast[Info]", "%s #  %d line  @ %s", file, line, msg);
			critical = false; 
			break;
		case Nv::Blast::ErrorCode::Enum::eDEBUG_INFO:		
			Logger::Debug("NvBlast[Debug Info]", "%s #  %d line  @ %s", file, line, msg);
			critical = false; 
			break;
		case Nv::Blast::ErrorCode::Enum::eDEBUG_WARNING:
			Logger::Warning("NvBlast[Debug Warning]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::eINVALID_PARAMETER:
			Logger::Debug("NvBlast[Invalid Parameter]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::eINVALID_OPERATION:
			Logger::Debug("NvBlast[Invalid Operation]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::eOUT_OF_MEMORY:
			Logger::Debug("NvBlast[Out of] Memory", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::eINTERNAL_ERROR:
			std::cout << "[Internal Error]";		
			Logger::Error("NvBlast[Debug Info]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::eABORT:				
			Logger::Debug("NvBlast[Abort]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		case Nv::Blast::ErrorCode::Enum::ePERF_WARNING:	
			Logger::Warning("NvBlast[Perf Warning]", "%s #  %d line  @ %s", file, line, msg);
			critical = false;
			break;
		default:							NVBLAST_ASSERT(false);
		}
#if NV_DEBUG || NV_CHECKED
		str << file << "(" << line << "): ";
#else 
		NV_UNUSED(file);
		NV_UNUSED(line);
#endif				
		if (critical)
		{
			Logger::Error("NvBlast", "Authoring failed. Exiting.");
			exit(-1);
		}
	}
};
#endif // BLAST_ERROR_CALLBACK_H
