/*
 * @Author       : HGL
 * @Date         : 2022-06-15 11:26:25
 * @LastEditTime : 2022-06-15 12:08:37
 * @Description  : 
 * @FilePath     : \Destrution\UI\UI.h
 */
#ifndef UI_H
#define UI_H

#ifdef _WINDOWS_
#include <wrl/client.h>
#endif

#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>

#include "../Renderer//Renderer.h"
#include "../Destruction/Common/Camera.h"
#include "../src/KG3D_DestructionMesh.h"
#include "../Destruction/Base.h"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

class UIDrawer:public Base{
public:
	UIDrawer(Application*app);
	~UIDrawer();
	bool InitUI();
	void Move(float dt);
	void Pick();
	void newFrame();
	void RenderUI();
	void UpdateUI();
	void drawMenuBar();
	void drawChunkTree(DestructionMesh* mesh);
	void drawFractureTool();
	void TravelChunkTree(DestructionMesh* mesh);
	void drawSceneWindow();
	void drawCascadeShadowWindow();
	LRESULT UIHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
private:
	void drawRandererControlPanel();

public:
	BlastActor* hitActor = nullptr;
	DestructionMesh* hitMesh = nullptr;
private:
	BlastManager* m_pBlastManager;

	bool show_chunk_tree=true;
	bool show_control_pannel=true;

	int camera_speed = 2;
	std::string m_PickedObjStr = "Nothing";

	//Blaster
	int cell_counts = 1;
};

#endif //UI_H