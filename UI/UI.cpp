#include "UI.h"
#include "../Destruction/Application.h"
#include "../Destruction/BlastAsset.h"
#include "../Destruction/BlastManager.h"
#include "../src/KG3D_DestructionMesh.h"
#include "../Destruction/Common/XUtil.h"
#include <Windows.h>
#include <DirectXColors.h>

using namespace DirectX;
UIDrawer::UIDrawer(Application*application)
{
	name = "UI Drawer";
	app = application;
}

UIDrawer::~UIDrawer() {
	DESTRUCTION_DELETE(hitActor);
	DESTRUCTION_DELETE(hitMesh);
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

bool UIDrawer::InitUI() {

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // 允许键盘控制
	io.ConfigWindowsMoveFromTitleBarOnly = true;              // 仅允许标题拖动

	// 设置Dear ImGui风格
	ImGui::StyleColorsDark();

	// 设置平台/渲染器后端
	ImGui_ImplWin32_Init(FindWindow(NULL,L"Destruction Editor"));
	ImGui_ImplDX11_Init(app->m_pRenderer->getD3DDevice(), app->m_pRenderer->getD3DDeviceContext());

	m_pBlastManager = app->m_pBlaster->GetBlastManager();

	return true;
}

void UIDrawer::Move(float dt) {
	// 更新摄像机
// 	if (app->m_pRenderer->m_CSManager.m_SelectedCamera <= CameraSelection::CameraSelection_Light)
// 		app->m_FPSCameraController.Update(dt);

	ImGuiIO& io = ImGui::GetIO();
	// ******************
	// 自由摄像机的操作
	//
	float d1 = 0.0f, d2 = 0.0f;
	if (ImGui::IsKeyDown('W'))
		d1 += dt;
	if (ImGui::IsKeyDown('S'))
		d1 -= dt;
	if (ImGui::IsKeyDown('A'))
		d2 -= dt;
	if (ImGui::IsKeyDown('D'))
		d2 += dt;
	
	app->m_pViewerCamera->MoveForward(d1 * 45.0f * camera_speed);
	app->m_pViewerCamera->Strafe(d2 * 45.0f * camera_speed);

	if (ImGui::IsMouseDragging(ImGuiMouseButton_Right))
	{
		app->m_pViewerCamera->Pitch(io.MouseDelta.y * 0.01f);
		app->m_pViewerCamera->RotateY(io.MouseDelta.x * 0.01f);
	}
}
void UIDrawer::Pick() {
	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureMouse) {
		Ray ray = Ray::ScreenToRay(*app->m_pViewerCamera.get(), (float)io.MousePos.x, (float)io.MousePos.y);
		m_PickedObjStr = "Nothing";
		hitActor = app->m_pBlaster->m_pBlastScene->GetActor(StringToID("..\\Model\\Rock\\RockFlatLong_Set.obj"));
		hitMesh = hitActor->GetModel()->m_pDestructionMesh[0];
		bool hitObject = false;
		if (ray.Hit(hitActor->GetModel()->m_pDestructionMesh[0]->m_pMeshData.m_BoundingBox))
		{
			m_PickedObjStr = "chunk0";
			hitActor;
			hitMesh = hitActor->GetModel()->m_pDestructionMesh[0];
			hitObject = true;
		}
		else if (ray.Hit(hitActor->GetModel()->m_pDestructionMesh[1]->m_pMeshData.m_BoundingBox))
		{
			m_PickedObjStr = "chunk1";
			hitActor;
			hitMesh = hitActor->GetModel()->m_pDestructionMesh[1];
			hitObject = true;
		}
		else if (ray.Hit(hitActor->GetModel()->m_pDestructionMesh[2]->m_pMeshData.m_BoundingBox))
		{
			m_PickedObjStr = "chunk2";
			hitActor;
			hitMesh = hitActor->GetModel()->m_pDestructionMesh[2];
			hitObject = true;
		}
		else if (ray.Hit(hitActor->GetModel()->m_pDestructionMesh[3]->m_pMeshData.m_BoundingBox))
		{
			m_PickedObjStr = "chunk3";
			hitActor;
			hitMesh = hitActor->GetModel()->m_pDestructionMesh[3];
			hitObject = true;
		}
		else if (ray.Hit(app->m_pBlaster->ground->m_pMeshData.m_BoundingBox)) {
			m_PickedObjStr = "ground";
			hitMesh = app->m_pBlaster->ground;
			hitActor;
			hitObject = true;
		}
		if (hitObject == true && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
		{
			hitActor->SetSelectedMesh(hitMesh);
			m_pBlastManager->SetSelectedActor(hitActor);
			//选中物体
			/*std::wstring wstr = L"你点击了";
			MessageBox(nullptr, wstr.c_str(), L"注意", 0);*/
		}
	}
}

void UIDrawer::newFrame() {
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
}

void UIDrawer::RenderUI() {
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

LRESULT UIDrawer::UIHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	return ImGui_ImplWin32_WndProcHandler(FindWindow(NULL, L"Destruction Editor"), msg, wParam, lParam);
}

void UIDrawer::UpdateUI() {
	Pick();
	drawRandererControlPanel();
	//drawSceneWindow();
	drawMenuBar();
	drawFractureTool();
	//drawCascadeShadowWindow();
}

void UIDrawer::drawRandererControlPanel() {
	ImGui::Begin("Renderer Control Panel",&show_control_pannel,ImGuiWindowFlags_NoCollapse);
	//ImGui::Checkbox("Use skyBox", 0);
	ImGui::SliderInt("Camera Speed", &camera_speed, 0, 10);
	float test1 = 1.0f;
	if (ImGui::SliderFloat("Explosion Amount", &test1, -10.0f, 10.0f))
	{
		XMFLOAT3 model_center= m_pBlastManager->GetSelectedActor()->GetModel()->m_pMeshData.m_BoundingBox.Center;
		for (auto it = m_pBlastManager->GetSelectedActor()->GetModel()->m_pDestructionMesh.begin();
			it != m_pBlastManager->GetSelectedActor()->GetModel()->m_pDestructionMesh.end(); it++)
		{
			(*it)->mesh_transform->SetPosition((*it)->base_displacement.x *test1, (*it)->base_displacement.y  * test1, (*it)->base_displacement.z  * test1);
		}
		//App->renderer3D->SetExplosionDisplacement(explosion_amount);
		//App->blast->explosion_amount = explosion_amount;
	}
	ImGui::End();
}
void UIDrawer::drawCascadeShadowWindow() {
	if (ImGui::Begin("Cascaded Shadow Mapping"))
	{
		ImGui::Checkbox("Debug Shadow", &app->m_pRenderer->m_DebugShadow);

		static bool visualizeCascades = false;
		if (ImGui::Checkbox("Visualize Cascades", &visualizeCascades))
		{
			app->m_pRenderer->m_pForwardEffect->SetCascadeVisulization(visualizeCascades);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}
		static const char* msaa_modes[] = {
			"None",
			"2x MSAA",
			"4x MSAA",
			"8x MSAA"
		};
		static int curr_msaa_item = 3;
		if (ImGui::Combo("MSAA", &curr_msaa_item, msaa_modes, ARRAYSIZE(msaa_modes)))
		{
			switch (curr_msaa_item)
			{
			case 0: app->m_pRenderer->m_MsaaSamples = 1; break;
			case 1: app->m_pRenderer->m_MsaaSamples = 2; break;
			case 2: app->m_pRenderer->m_MsaaSamples = 4; break;
			case 3: app->m_pRenderer->m_MsaaSamples = 8; break;
			}
			DXGI_SAMPLE_DESC sampleDesc;
			sampleDesc.Count = app->m_pRenderer->m_MsaaSamples;
			sampleDesc.Quality = 0;
			app->m_pRenderer->m_pLitBuffer = std::make_unique<Texture2D>(app->m_pRenderer->getD3DDevice(), app->m_pRenderer->m_ClientWidth, app->m_pRenderer->m_ClientHeight, DXGI_FORMAT_R8G8B8A8_UNORM,
				D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE, sampleDesc);
			app->m_pRenderer->m_pDepthBuffer = std::make_unique<Depth2D>(app->m_pRenderer->getD3DDevice(), app->m_pRenderer->m_ClientWidth, app->m_pRenderer->m_ClientHeight,
				D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE, sampleDesc);
			app->m_pRenderer->m_pSkyboxEffect->SetMsaaSamples(app->m_pRenderer->m_MsaaSamples);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}


		static int texture_level = 10;
		ImGui::Text("Texture Size: %d", app->m_pRenderer->m_CSManager.m_ShadowSize);
		if (ImGui::SliderInt("##0", &texture_level, 9, 13, ""))
		{
			app->m_pRenderer->m_CSManager.m_ShadowSize = (1 << texture_level);
			app->m_pRenderer->m_CSManager.InitResource(app->m_pRenderer->getD3DDevice());
			//app->m_pRenderer->m_pDebugShadowBuffer = std::make_unique<Texture2D>(app->m_pRenderer->getD3DDevice(), app->m_pRenderer->m_CSManager.m_ShadowSize, app->m_pRenderer->m_CSManager.m_ShadowSize, DXGI_FORMAT_R8G8B8A8_UNORM);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}



		static int pcf_kernel_level = 1;
		ImGui::Text("PCF Kernel Size: %d", app->m_pRenderer->m_CSManager.m_PCFKernelSize);
		if (ImGui::SliderInt("##1", &pcf_kernel_level, 0, 15, ""))
		{
			app->m_pRenderer->m_CSManager.m_PCFKernelSize = 2 * pcf_kernel_level + 1;
			app->m_pRenderer->m_pForwardEffect->SetPCFKernelSize(app->m_pRenderer->m_CSManager.m_PCFKernelSize);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}

		ImGui::Text("Depth Offset");
		if (ImGui::SliderFloat("##2", &app->m_pRenderer->m_CSManager.m_PCFDepthOffset, 0.0f, 0.05f))
		{
			app->m_pRenderer->m_pForwardEffect->SetPCFDepthOffset(app->m_pRenderer->m_CSManager.m_PCFDepthOffset);
		}

		if (ImGui::Checkbox("Cascade Blur", &app->m_pRenderer->m_CSManager.m_BlendBetweenCascades))
		{
			app->m_pRenderer->m_pForwardEffect->SetCascadeBlendEnabled(app->m_pRenderer->m_CSManager.m_BlendBetweenCascades);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}
		if (ImGui::SliderFloat("##3", &app->m_pRenderer->m_CSManager.m_BlendBetweenCascadesRange, 0.0f, 0.5f))
		{
			app->m_pRenderer->m_pForwardEffect->SetCascadeBlendArea(app->m_pRenderer->m_CSManager.m_BlendBetweenCascadesRange);
		}

		if (ImGui::Checkbox("DDX, DDY offset", &app->m_pRenderer->m_CSManager.m_DerivativeBasedOffset))
		{
			app->m_pRenderer->m_pForwardEffect->SetPCFDerivativesOffsetEnabled(app->m_pRenderer->m_CSManager.m_DerivativeBasedOffset);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}
		if (ImGui::Checkbox("Fixed Size Frustum AABB", &app->m_pRenderer->m_CSManager.m_FixedSizeFrustumAABB))
			app->m_pRenderer->need_gpu_timer_reset = true;
		if (ImGui::Checkbox("Fit Light to Texels", &app->m_pRenderer->m_CSManager.m_MoveLightTexelSize))
			app->m_pRenderer->need_gpu_timer_reset = true;

		static const char* fit_projection_strs[] = {
			"Fit Projection To Cascade",
			"Fit Projection To Scene"
		};
		if (ImGui::Combo("##4", reinterpret_cast<int*>(&app->m_pRenderer->m_CSManager.m_SelectedCascadesFit), fit_projection_strs, ARRAYSIZE(fit_projection_strs)))
			app->m_pRenderer->need_gpu_timer_reset = true;

		static const char* camera_strs[] = {
			"Main Camera",
			"Light Camera",
			"Cascade Camera 1",
			"Cascade Camera 2",
			"Cascade Camera 3",
			"Cascade Camera 4",
			"Cascade Camera 5",
			"Cascade Camera 6",
			"Cascade Camera 7",
			"Cascade Camera 8"
		};
		static int camera_idx = 0;
		if (camera_idx > app->m_pRenderer->m_CSManager.m_CascadeLevels + 2)
			camera_idx = app->m_pRenderer->m_CSManager.m_CascadeLevels + 2;
		if (ImGui::Combo("##5", &camera_idx, camera_strs, app->m_pRenderer->m_CSManager.m_CascadeLevels + 2))
		{
			app->m_pRenderer->m_CSManager.m_SelectedCamera = static_cast<CameraSelection>(camera_idx);
			if (app->m_pRenderer->m_CSManager.m_SelectedCamera == CameraSelection::CameraSelection_Eye)
			{
				app->m_FPSCameraController.InitCamera(static_cast<FirstPersonCamera*>(app->m_pViewerCamera.get()));
				app->m_FPSCameraController.SetMoveSpeed(10.0f);
			}
			else if (app->m_pRenderer->m_CSManager.m_SelectedCamera == CameraSelection::CameraSelection_Light)
			{
				app->m_FPSCameraController.InitCamera(static_cast<FirstPersonCamera*>(app->m_pLightCamera.get()));
				app->m_FPSCameraController.SetMoveSpeed(50.0f);
			}
		}

		static const char* fit_near_far_strs[] = {
			"0:1 NearFar",
			"Cascade AABB NearFar",
			"Scene AABB NearFar",
			"Scene AABB Intersection NearFar"
		};

		if (ImGui::Combo("##6", reinterpret_cast<int*>(&app->m_pRenderer->m_CSManager.m_SelectedNearFarFit), fit_near_far_strs, ARRAYSIZE(fit_near_far_strs)))
			app->m_pRenderer->need_gpu_timer_reset = true;

		static const char* cascade_selection_strs[] = {
			"Map-based Selection",
			"Interval-based Selection",
		};
		if (ImGui::Combo("##7", reinterpret_cast<int*>(&app->m_pRenderer->m_CSManager.m_SelectedCascadeSelection), cascade_selection_strs, ARRAYSIZE(cascade_selection_strs)))
		{
			app->m_pRenderer->m_pForwardEffect->SetCascadeIntervalSelectionEnabled(static_cast<bool>(app->m_pRenderer->m_CSManager.m_SelectedCascadeSelection));
			app->m_pRenderer->need_gpu_timer_reset = true;
		}

		static const char* cascade_levels[] = {
			"1 Level",
			"2 Levels",
			"3 Levels",
			"4 Levels",
			"5 Levels",
			"6 Levels",
			"7 Levels",
			"8 Levels"
		};
		static int cascade_level_idx = app->m_pRenderer->m_CSManager.m_CascadeLevels - 1;
		if (ImGui::Combo("Cascade", &cascade_level_idx, cascade_levels, ARRAYSIZE(cascade_levels)))
		{
			app->m_pRenderer->m_CSManager.m_CascadeLevels = cascade_level_idx + 1;
			app->m_pRenderer->m_CSManager.InitResource(app->m_pRenderer->getD3DDevice());
			app->m_pRenderer->m_pForwardEffect->SetCascadeLevels(app->m_pRenderer->m_CSManager.m_CascadeLevels);
			app->m_pRenderer->need_gpu_timer_reset = true;
		}

		char level_str[] = "Level1";
		for (int i = 0; i < app->m_pRenderer->m_CSManager.m_CascadeLevels; ++i)
		{
			level_str[5] = '1' + i;
			ImGui::SliderFloat(level_str, app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage + i, 0.0f, 1.0f, "");
			ImGui::SameLine();
			ImGui::Text("%.1f%%", app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] * 100);
			if (i && app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] < app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i - 1])
				app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] = app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i - 1];
			if (i < app->m_pRenderer->m_CSManager.m_CascadeLevels - 1 && app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] > app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i + 1])
				app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] = app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i + 1];
			if (app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] > 1.0f)
				app->m_pRenderer->m_CSManager.m_CascadePartitionsPercentage[i] = 1.0f;
		}

	}
	ImGui::End();
}

void UIDrawer::drawSceneWindow() {
	ImGui::Begin("Scene");
	ImVec2 pos = ImGui::GetCursorScreenPos();
	ImVec2 winSize = ImGui::GetWindowSize();
	app->m_pRenderer->LoadSceneBuffer();
	float smaller = (std::min)((winSize.x - 20) / app->AspectRatio(), winSize.y - 36);
	ImGui::Image(app->m_pRenderer->m_pSceneBuffer->GetShaderResource(), ImVec2(smaller * app->AspectRatio(), smaller));
	ImGui::End();
}
void UIDrawer::drawMenuBar() {
	if (ImGui::BeginMainMenuBar())
	{
		ImGuiStyle& style = ImGui::GetStyle();
		ImVec4 previous_color = style.Colors[ImGuiCol_Text];
		style.Colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
		if (ImGui::BeginMenu("File"))
		{
			style.Colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

			if (ImGui::MenuItem("Save File")) {
				//App->blast->CreateBlastFile();
			}

			ImGui::EndMenu();
			style.Colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
		}
		
		if (ImGui::BeginMenu("Run"))
		{
			style.Colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

			if (ImGui::MenuItem("Run"))
			{
				
			}
			if (ImGui::MenuItem("Pause"))
			{
				
			}
			if (ImGui::MenuItem("Replay"))
			{
				
			}
			ImGui::EndMenu();
			style.Colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
		}

		if (ImGui::BeginMenu("Tools"))
		{
			style.Colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

			if (ImGui::MenuItem("Chunk Tree"))
			{

			}
			if (ImGui::MenuItem("Fracture Tool"))
			{

			}
			ImGui::EndMenu();
			style.Colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
		}
		ImGui::EndMainMenuBar();
		style.Colors[ImGuiCol_Text] = previous_color;
	}
}

void UIDrawer::drawChunkTree(DestructionMesh* mesh)
{
	ImGui::Begin("Chunk Tree");
	if (mesh)
	{
		UINT flag = 0;
		std::string name = "Chunk_id : Chunk_Depth ";
		if (!mesh->is_root) flag |= ImGuiTreeNodeFlags_Leaf;
		flag |= ImGuiTreeNodeFlags_OpenOnArrow;
		//if (mesh == app->blast->current_selected_mesh) flag |= ImGuiTreeNodeFlags_Selected;
		ImGui::Text(name.data());

		if (ImGui::BeginPopupContextWindow("Options"))
		{
			if (ImGui::MenuItem("Remove Childs"))
			{
				m_pBlastManager->RemoveChildChunks(m_pBlastManager->GetSelectedActor()->GetSelectedMesh());
			}
			ImGui::EndPopup();
		}

		for (auto it = mesh->m_pDestructionMesh.begin(); it != mesh->m_pDestructionMesh.end(); it++)
			TravelChunkTree(*it);
	}
	else {
		ImGui::Text("Select A Target First!");
	}
	ImGui::End();
}

void UIDrawer::TravelChunkTree(DestructionMesh* mesh) {
	UINT flag = 0;
	std::string name = "Chunk " + std::to_string(mesh->chunk_id) + " : Depth " + std::to_string(mesh->chunk_depth);
	if (!mesh->is_root) flag |= ImGuiTreeNodeFlags_Leaf;
	flag |= ImGuiTreeNodeFlags_OpenOnArrow;
	//if (mesh == app->blast->current_selected_mesh) flag |= ImGuiTreeNodeFlags_Selected;

	if (ImGui::TreeNodeEx(name.c_str(), flag))
	{
		if (ImGui::IsItemHovered())
		{
			if (ImGui::IsMouseClicked(ImGuiMouseButton_Left) || ImGui::IsMouseClicked(ImGuiMouseButton_Left))
			{
				m_pBlastManager->SetCurrentMesh(mesh->blast_mesh);
				m_pBlastManager->GetSelectedActor()->SetSelectedMesh(mesh);
				ImGui::OpenPopupOnItemClick("Options");
			}
		}
		for (auto it = mesh->m_pDestructionMesh.begin(); it != mesh->m_pDestructionMesh.end(); it++)
			TravelChunkTree(*it);

		ImGui::TreePop();
	}
}

void UIDrawer::drawFractureTool() {
	ImGui::Begin("Fracture Tool"); 

	ImGui::Text("Picking:%s", m_PickedObjStr.data());
	if (m_pBlastManager->GetSelectedActor()) {
		if (m_pBlastManager->GetSelectedActor())
			ImGui::Text("Current Selected Actor: Actor id%d",
				m_pBlastManager->GetSelectedActor()->actor_ID);
		if (m_pBlastManager->GetSelectedActor()->GetSelectedMesh())
			ImGui::Text("Current Selected Chunk: chunk%d",
				m_pBlastManager->GetSelectedActor()->GetSelectedMesh()->chunk_id);
	}
	else
		ImGui::Text("None Current Selected Chunk");

	if (ImGui::Button("Create Blast Mesh"))
	{
		m_pBlastManager->CreateBlastModel();
	}
	ImGui::Separator();
	if (ImGui::Button("Fracture"))
	{
		if (m_pBlastManager->GetSelectedActor())
			m_pBlastManager->ApplyFracture();
	}
	ImGui::SameLine();
	if (ImGui::Button("Reset"))
	{

	}
	ImGui::SameLine();
	if (ImGui::Button("Optimize"))
	{

	}
	ImGui::Separator();

	const char* fracture_names[6] = { "Voronoi Uniform", "Voronoi Clustered", "Voronoi Radial", "Voronoi In Sphere", "Voronoi Remove In Sphere", "Uniform Slicing" };
	static const char* current_item = fracture_names[0];
	ImGui::Text("Fracture Type:");
	if (ImGui::BeginCombo("##fracture_type", current_item))
	{
		for (int i = 0; i < 6; i++)
		{
			if (ImGui::Selectable(fracture_names[i]))
			{
				m_pBlastManager->SetFractureType(BlastManager::FractureType::VoronoiUniform);
				current_item = fracture_names[i];
			}
		}
		ImGui::EndCombo();
	}

	cell_counts = m_pBlastManager->GetCellCounts();
	ImGui::Text("Cell Count:");
	if (ImGui::DragInt("##Cell Count", &cell_counts, 1, 100))
	{
		m_pBlastManager->SetCellCounts(cell_counts);
	}
	ImGui::End();
}