#include "PhysicsModule.h"
#include "../Logger/Logger.h"
#include <NvBlastPxCallbacks.h>
namespace {
	PhysicsModule * Physics_Instance= nullptr;
}

PhysicsModule::PhysicsModule(Application* application):
	m_pScene(nullptr),
	m_pPhysics(nullptr),
	m_pPvd(nullptr),
	m_pMaterial(nullptr),
	m_pCooking(nullptr),
	m_pFoundation(nullptr)
{
	name = "Physics Module";
	app = application;
	if (Physics_Instance)
		throw std::exception("MeshImporter is a singleton!");
	Physics_Instance = this;
}

PhysicsModule:: ~PhysicsModule(){

}

PhysicsModule* PhysicsModule::Get() {
	if (!Physics_Instance)
		throw std::exception("Physics needs an instance");
	return Physics_Instance;
}

bool PhysicsModule::Init() {
	bool nResult = true;
	m_pFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, NvBlastGetPxAllocatorCallback(), NvBlastGetPxErrorCallback());
	if (!m_pFoundation)
	{
		Logger::Error(name, "%s Can't init PhysX foundation! %s", FUNCTION_PATH);
		nResult = false;
		return nResult;
	}
	physx::PxTolerancesScale scale;
	m_pPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_pFoundation, scale, true);
	if (!m_pPhysics)
	{
		Logger::Error(name, "%s Can't create Physics! %s", FUNCTION_PATH);
		nResult = false;
		return nResult;
	}
	physx::PxCookingParams cookingParams(scale);
	cookingParams.buildGPUData = true;
	m_pCooking = PxCreateCooking(PX_PHYSICS_VERSION, m_pPhysics->getFoundation(), cookingParams);
	if (!m_pCooking)
	{
		Logger::Error(name, "%s Can't create Cooking! %s", FUNCTION_PATH);
		nResult = false;
		return nResult;
	}
	return nResult;
}

bool PhysicsModule::Update() {
	//m_pScene->simulate(1.0f / 60.0f);
	//m_pScene->fetchResults(true);
	return true;
}

physx::PxPhysics* PhysicsModule::GetPxPhysics() {
	return m_pPhysics;
}

physx::PxFoundation* PhysicsModule::GetPxFoundation() {
	return m_pFoundation;
}

physx::PxCooking* PhysicsModule::GetPxCooking() {
	return m_pCooking;
}