#ifndef PHYSICS_MODULE
#define PHYSICS_MODULE
#include <PxPhysicsAPI.h>
#include "../Destruction/Base.h"
namespace physx {
	class PxPhysics;
	class PxFoundation;
	class PxCooking;
	class PxDefaultErrorCallback;
	class PxDefaultAllocator;
}
class Application;
class PhysicsModule:public Base
{
public:
	PhysicsModule(Application* application);
	~PhysicsModule();
	PhysicsModule(PhysicsModule&) = delete;
	PhysicsModule& operator=(const PhysicsModule&) = delete;
	PhysicsModule(PhysicsModule&&) = default;
	PhysicsModule& operator=(PhysicsModule&&) = default;

	static PhysicsModule* Get();

	bool Init();
	bool Update();

	physx::PxFoundation* GetPxFoundation();
	physx::PxPhysics* GetPxPhysics();
	physx::PxCooking* GetPxCooking();

private:
	physx::PxFoundation* m_pFoundation;
	physx::PxPhysics* m_pPhysics;
	physx::PxCooking* m_pCooking;
	physx::PxPvd* m_pPvd;
	physx::PxScene* m_pScene;
	physx::PxMaterial* m_pMaterial;
	physx::PxDefaultCpuDispatcher* m_pDefaultCpuDispatcher;
};

#endif // PHYSICS_MODULE