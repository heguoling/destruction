/*
 * @Author       : HGL
 * @Date         : 2022-06-17 14:53:26
 * @LastEditTime : 2022-07-15 13:45:48
 * @Description  : 
 * @FilePath     : \Destrution\Importer\MeshImporter.h
 */
#ifndef MESH_IMPORTER
#define MESH_IMPORTER
#include "../Destruction/Common/WinMin.h"
#include "../Destruction/Common/Geometry.h"
#include "../Destruction/Common/Material.h"
#include "../Destruction/Common/MeshData.h"
#include <d3d11_1.h>
#include "../Destruction/Common/XUtil.h"
#include <wrl/client.h>
#include "../src/KG3D_DestructionMesh.h"
#include "../Destruction/Base.h"
namespace KG3D_Destruction {
    class DestructionMesh;
}
struct Model
{
    std::vector<Material*> materials;
    KG3D_Destruction::DestructionMesh* m_pMesh = new KG3D_Destruction::DestructionMesh();
    DirectX::BoundingBox boundingbox;
};


class MeshImporter:public Base
{
public:
    MeshImporter(Application*application);
    ~MeshImporter();
    MeshImporter(MeshImporter&) = delete;
    MeshImporter& operator=(const MeshImporter&) = delete;
    MeshImporter(MeshImporter&&) = default;
    MeshImporter& operator=(MeshImporter&&) = default;

    /**
     * @brief Get singleton MeshImporter
     * 
     * @return MeshImporter& 
     */
    static MeshImporter& Get();

    /**
     * @brief Init the MeshImporter
     * 
     * @param device 
     * @return true 
     * @return false 
     */
    bool Init(ID3D11Device* device);
    
    /**
     * @brief Create a Mesh From File
     * 
     * @param filename 
     * @return const Model* 
     */
    const Model* CreateFromFile(std::string_view filename);

    /**
     * @brief Create a Mesh From Geometry
     * 
     * @param name 
     * @param data 
     * @return const Model* 
     */
    const Model* CreateFromGeometry(std::string_view name, const Geometry::MeshData& data);

    /**
     * @brief Get the Model according to name
     * 
     * @param name 
     * @return const Model* 
     */
    const Model* GetModel(std::string_view name) const;

    /**
     * @brief Get the Model according to name
     * 
     * @param name 
     * @return Model* 
     */
    Model* GetModel(std::string_view name);

    /**
     * @brief Get the Model according to ID
     * 
     * @param nameID 
     * @return const Model* 
     */
    const Model* GetModel(XID nameID) const;

    /**
     * @brief Get the Model according to name
     * 
     * @param nameID 
     * @return Model* 
     */
    Model* GetModel(XID nameID);

private:
    Microsoft::WRL::ComPtr<ID3D11Device> m_pDevice;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_pDeviceContext;
    std::unordered_map<size_t, Model> m_Models;
};

#endif //MESH_IMPORTER

