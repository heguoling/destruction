#include "MeshImporter.h"
#include "../Destruction/Common/Transform.h"
#include "../Destruction/Texture.h"
#include "../Destruction/Common/XUtil.h"
#include "../src/KG3D_DestructionMesh.h"
#include "../Destruction/Application.h"
#include "../Destruction/Texture.h"
#include "../Destruction/BlastManager.h"
#include "TextureImporter.h"


#include "NvBlastExtAuthoring.h"

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/cfileio.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <Assimp/cimport.h>
#include "../Destruction/Common/XUtil.h"
#include "MeshImporter.h"
#include "TextureImporter.h"

#include <filesystem>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

using namespace DirectX;

namespace
{
    // ModelManager单例
    MeshImporter* s_pInstance = nullptr;
}


MeshImporter::MeshImporter(Application*application)
{
    name = "Mesh Importer";
    app = application;
    if (s_pInstance)
        throw std::exception("MeshImporter is a singleton!");
    s_pInstance = this;
}

MeshImporter::~MeshImporter()
{
}

MeshImporter& MeshImporter::Get()
{
    if (!s_pInstance)
        throw std::exception("MeshImporter needs an instance!");
    return *s_pInstance;
}

bool MeshImporter::Init(ID3D11Device* device)
{
    m_pDevice = device;
    m_pDevice->GetImmediateContext(m_pDeviceContext.ReleaseAndGetAddressOf());
    return true;
}

const Model* MeshImporter::CreateFromFile(std::string_view filename)
{
    Logger::Debug(name,"导入模型:%s", std::string{ filename }.c_str());
    XID modelID = StringToID(filename);
    if (m_Models.count(modelID))
        return &m_Models[modelID];

    using namespace Assimp;
    namespace fs = std::filesystem;
    Importer importer;

    auto pAssimpScene = importer.ReadFile(filename.data(), aiProcess_ConvertToLeftHanded | aiProcess_SortByPType|
         aiProcess_Triangulate | aiProcess_ImproveCacheLocality);

    if (pAssimpScene && !(pAssimpScene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) && pAssimpScene->HasMeshes())
    {
        auto& model = m_Models[modelID];
        model.m_pMesh->name = filename;
        //model.m_pMesh->m_pDestructionMesh.resize(pAssimpScene->mNumMeshes);
        model.m_pMesh->m_pDestructionMesh = std::vector<DestructionMesh*>(pAssimpScene->mNumMeshes);
        model.materials = std::vector < Material*> (pAssimpScene->mNumMaterials);

        for (UINT i = 0; i < pAssimpScene->mNumMeshes; ++i)
        {
			model.m_pMesh->m_pDestructionMesh[i] = new DestructionMesh();
            auto& mesh = model.m_pMesh->m_pDestructionMesh[i];

            auto pAiMesh = pAssimpScene->mMeshes[i];
            UINT numVertices = pAiMesh->mNumVertices;

            CD3D11_BUFFER_DESC bufferDesc(0, D3D11_BIND_VERTEX_BUFFER);
            D3D11_SUBRESOURCE_DATA initData{ nullptr, 0, 0 };
            // 位置
            if (pAiMesh->mNumVertices > 0)
            {
                initData.pSysMem = pAiMesh->mVertices;
				mesh->m_pMeshData.m_VertexCount = pAiMesh->mNumVertices;
				NvcVec3* vertices = (NvcVec3*)pAiMesh->mVertices;
                mesh->vertices = new NvcVec3[numVertices];
                memcpy_s(mesh->vertices, sizeof(NvcVec3)*mesh->m_pMeshData.m_VertexCount,
                    pAiMesh->mVertices, sizeof(aiVector3D) * mesh->m_pMeshData.m_VertexCount);
				for (size_t i = 0; i < numVertices; ++i) {
					if (mesh->vertices[i].x != pAiMesh->mVertices[i].x && mesh->vertices[i].y != pAiMesh->mVertices[i].y && mesh->vertices[i].z != pAiMesh->mVertices[i].z)
                        Logger::Debug("debug", "不相等");
				}
                bufferDesc.ByteWidth = numVertices * sizeof(XMFLOAT3);
                m_pDevice->CreateBuffer(&bufferDesc, &initData, mesh->m_pMeshData.m_pVertices.GetAddressOf());
                BoundingBox::CreateFromPoints(mesh->m_pMeshData.m_BoundingBox, numVertices,
                    (const XMFLOAT3*)pAiMesh->mVertices, sizeof(XMFLOAT3));
                if (i == 0)
                    model.boundingbox = mesh->m_pMeshData.m_BoundingBox;
                else
                    model.boundingbox.CreateMerged(model.boundingbox, model.boundingbox, mesh->m_pMeshData.m_BoundingBox);
            }

            // 法线
            if (pAiMesh->HasNormals())
            {
                initData.pSysMem = pAiMesh->mNormals;
                mesh->normals = new NvcVec3[numVertices];
				memcpy_s(mesh->normals, sizeof(NvcVec3) * mesh->m_pMeshData.m_VertexCount,
                    pAiMesh->mNormals, sizeof(aiVector3D) * mesh->m_pMeshData.m_VertexCount);
                bufferDesc.ByteWidth = numVertices * sizeof(XMFLOAT3);
                m_pDevice->CreateBuffer(&bufferDesc, &initData, mesh->m_pMeshData.m_pNormals.GetAddressOf());
            }

            // 切线
            if (pAiMesh->HasTangentsAndBitangents())
            {
                std::vector<XMFLOAT4> tangents(numVertices, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
                for (UINT i = 0; i < pAiMesh->mNumVertices; ++i)
                {
                    memcpy_s(&tangents[i], sizeof(XMFLOAT3),
                        pAiMesh->mTangents + i, sizeof(XMFLOAT3));
                }

                initData.pSysMem = tangents.data();
                mesh->tangents = (float*)tangents.data();
                bufferDesc.ByteWidth = pAiMesh->mNumVertices * sizeof(XMFLOAT4);
                m_pDevice->CreateBuffer(&bufferDesc, &initData, mesh->m_pMeshData.m_pTangents.GetAddressOf());
            }

            // 纹理坐标
            UINT numUVs = 8;
            while (numUVs && !pAiMesh->HasTextureCoords(numUVs - 1))
                numUVs--;
			NvcVec2* uv = new NvcVec2[numUVs * numVertices*2];
			if (numUVs > 0)
			{
				mesh->m_pMeshData.m_pTexcoordArrays.resize(numUVs);
				for (UINT i = 0; i < numUVs; ++i)
				{
					std::vector<XMFLOAT2> uvs(numVertices);
					for (UINT j = 0; j < numVertices; ++j)
					{
						uv[i * numUVs + j].x = pAiMesh->mTextureCoords[i]->x + j;
						uv[i * numUVs + j].y = pAiMesh->mTextureCoords[i]->y + j;
						memcpy_s(&uvs[j], sizeof(XMFLOAT2),
							pAiMesh->mTextureCoords[i] + j, sizeof(XMFLOAT2));

					}
                    initData.pSysMem = uvs.data();
                    bufferDesc.ByteWidth = numVertices * sizeof(XMFLOAT2);
                    m_pDevice->CreateBuffer(&bufferDesc, &initData, mesh->m_pMeshData.m_pTexcoordArrays[i].GetAddressOf());
                }
            }
            mesh->texture_coords = (NvcVec2*)uv;
            // 索引
            UINT numFaces = pAiMesh->mNumFaces;
           
            UINT numIndices = numFaces * 3;
            mesh->m_pMeshData.m_IndexCount = numFaces * 3;
            UINT* m_indices = nullptr;
            if (numFaces > 0)
            {
                m_indices = new UINT[numIndices];
                mesh->m_pMeshData.m_IndexCount = numIndices;
                    std::vector<UINT> indices(numIndices);
                    for (size_t i = 0; i < numFaces; ++i)
                    {
                        memcpy_s(indices.data() + i * 3, sizeof(UINT) * 3,
                            pAiMesh->mFaces[i].mIndices, sizeof(UINT) * 3);
                    }
                    bufferDesc = CD3D11_BUFFER_DESC(numIndices * sizeof(UINT), D3D11_BIND_INDEX_BUFFER);
                    initData.pSysMem = indices.data();
					mesh->indices = new UINT[numIndices];
					memcpy_s(mesh->indices, sizeof(UINT)*numIndices, indices.data(), sizeof(UINT)*numIndices);
                    m_pDevice->CreateBuffer(&bufferDesc, &initData, mesh->m_pMeshData.m_pIndices.GetAddressOf());
            }
            mesh->m_pMeshData.m_MaterialIndex = pAiMesh->mMaterialIndex;
			mesh->chunk_id = i;
			//mesh->chunk_depth = 0;
            mesh->mesh_id = modelID + mesh->chunk_id;
            mesh->name = "chunk" + std::to_string(mesh->chunk_id);

        }


        for (UINT i = 0; i < pAssimpScene->mNumMaterials; ++i)
        {
			model.materials[i] = new Material();
            auto& material = model.materials[i];

            auto pAiMaterial = pAssimpScene->mMaterials[i];
            XMFLOAT4 vec{};
            float value{};
            UINT boolean{};
            UINT num = 3;

            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_AMBIENT, (float*)&vec, &num))
                material->Set("$AmbientColor", vec);
            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, (float*)&vec, &num))
                material->Set("$DiffuseColor", vec);
            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_SPECULAR, (float*)&vec, &num))
                material->Set("$SpecularColor", vec);
            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_EMISSIVE, (float*)&vec, &num))
                material->Set("$EmissiveColor", vec);
            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_TRANSPARENT, (float*)&vec, &num))
                material->Set("$TransparentColor", vec);
            if (aiReturn_SUCCESS == pAiMaterial->Get(AI_MATKEY_COLOR_REFLECTIVE, (float*)&vec, &num))
                material->Set("$ReflectiveColor", vec);
            if (pAiMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
            {
                aiString aiPath;
                pAiMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &aiPath);
                fs::path tex_filename = filename;
                tex_filename = tex_filename.parent_path() / aiPath.C_Str();
                TextureImporter::Get().CreateTexture(tex_filename.string(), true, true);
                material->SetTexture("$Diffuse", tex_filename.string());
            }
            if (pAiMaterial->GetTextureCount(aiTextureType_NORMALS) > 0)
            {
                aiString aiPath;
                pAiMaterial->GetTexture(aiTextureType_NORMALS, 0, &aiPath);
                fs::path tex_filename = filename;
                tex_filename = tex_filename.parent_path() / aiPath.C_Str();
                TextureImporter::Get().CreateTexture(tex_filename.string());
                material->SetTexture("$Normal", tex_filename.string());
            }
        }
        Logger::Debug(name, "导入模型成功%s", std::string{ filename }.c_str());
        return &model;
    }

	

    return nullptr;
}

const Model* MeshImporter::CreateFromGeometry(std::string_view name, const Geometry::MeshData& data)
{
    XID modelID = StringToID(name);
    if (m_Models.count(modelID))
        return nullptr;

    auto& model = m_Models[modelID];

    model.m_pMesh->m_pDestructionMesh=std::vector<DestructionMesh*>(1,new DestructionMesh());
    model.materials.resize(1);
    model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pTexcoordArrays.resize(1);
    model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_IndexCount = (UINT)(data.indices32.size());
    model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_MaterialIndex = 0;
    CD3D11_BUFFER_DESC bufferDesc(0, D3D11_BIND_VERTEX_BUFFER);
    D3D11_SUBRESOURCE_DATA initData{ nullptr, 0, 0 };

    initData.pSysMem = data.vertices.data();
    bufferDesc.ByteWidth = (UINT)data.vertices.size() * sizeof(XMFLOAT3);
    m_pDevice->CreateBuffer(&bufferDesc, &initData, model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pVertices.ReleaseAndGetAddressOf());

    initData.pSysMem = data.normals.data();
    bufferDesc.ByteWidth = (UINT)data.normals.size() * sizeof(XMFLOAT3);
    m_pDevice->CreateBuffer(&bufferDesc, &initData, model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pNormals.ReleaseAndGetAddressOf());

    initData.pSysMem = data.texcoords.data();
    bufferDesc.ByteWidth = (UINT)data.texcoords.size() * sizeof(XMFLOAT2);
    m_pDevice->CreateBuffer(&bufferDesc, &initData, model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pTexcoordArrays[0].ReleaseAndGetAddressOf());

    initData.pSysMem = data.tangents.data();
    bufferDesc.ByteWidth = (UINT)data.tangents.size() * sizeof(XMFLOAT4);
    m_pDevice->CreateBuffer(&bufferDesc, &initData, model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pTangents.ReleaseAndGetAddressOf());

    initData.pSysMem = data.indices32.data();
    bufferDesc = CD3D11_BUFFER_DESC((UINT)data.indices32.size() * sizeof(UINT), D3D11_BIND_INDEX_BUFFER);
    m_pDevice->CreateBuffer(&bufferDesc, &initData, model.m_pMesh->m_pDestructionMesh[0]->m_pMeshData.m_pIndices.ReleaseAndGetAddressOf());

    return &model;
}


const Model* MeshImporter::GetModel(std::string_view name) const
{
    XID nameID = StringToID(name);
    if (auto it = m_Models.find(nameID); it != m_Models.end())
        return &it->second;
    return nullptr;
}

Model* MeshImporter::GetModel(std::string_view name)
{
    XID nameID = StringToID(name);
    if (m_Models.count(nameID))
        return &m_Models[nameID];
    return nullptr;
}

const Model* MeshImporter::GetModel(XID nameID) const
{
	if (auto it = m_Models.find(nameID); it != m_Models.end())
		return &it->second;
	return nullptr;
}

Model* MeshImporter::GetModel(XID nameID)
{
	if (m_Models.count(nameID))
		return &m_Models[nameID];
	return nullptr;
}

