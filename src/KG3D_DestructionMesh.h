/*
 * @Author       : HGL
 * @Date         : 2022-07-11 11:09:11
 * @LastEditTime : 2022-07-15 14:37:15
 * @Description  :
 * @FilePath     : \Destrution\src\KG3D_DestructionMesh.h
 */

#ifndef KG3D_DESTRUCTION_MESH
#define KG3D_DESTRUCTION_MESH
#include "KG3D_Destruction.h"
#include "../Destruction/Common/Transform.h"
#include "../Destruction/Common/XUtil.h"
#include "../Destruction/Common/MeshData.h"
using namespace Nv::Blast;
class Material;
struct MeshData;
namespace KG3D_Destruction
{
    class DestructionMesh
    {
    public:
        DestructionMesh();
        virtual ~DestructionMesh();

        /**
         * @brief 创建顶点
         *
         */
        void CreateVerticesFromData();

        /**
         * @brief 设置爆炸偏移量
         *
         * @param displacement
         */
        void SetExplosionDisplacement(FLOAT displacement);

    public:
        UINT *indices;           //索引数据
        NvcVec3 *normals;        //法线数据
        FLOAT *colors;           //顶点颜色数据
        NvcVec2 *texture_coords; //纹理坐标数据
        FLOAT *tangents;
        NvcVec3 *vertices = nullptr;       //顶点数据
        Transform *mesh_transform;         // mesh Transform
        std::vector<Material *> materials; //材质数组 最后的材质为InnerMaterial
        XMFLOAT3 base_displacement;
        UINT chunk_id;
        UINT chunk_depth;
        Nv::Blast::Mesh *blast_mesh = nullptr;
        Graphics::MeshData m_pMeshData;
        BOOL is_root;
        std::vector<DestructionMesh *> m_pDestructionMesh;
        // BoundingBox m_pBoudingBox;
        std::string name;
        XID mesh_id;
    };
}

#endif // KG3D_DESTRUCTION_MESH