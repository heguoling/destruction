/*
 * @Author       : HGL
 * @Date         : 2022-06-13 16:54:59
 * @LastEditTime : 2022-07-06 15:10:23
 * @Description  : 
 * @FilePath     : \Destrution\src\KG3D_DestructionMesh.cpp
 */
#include "KG3D_DestructionMesh.h"
#include "NvBlastExtAuthoringMesh.h"

#include "../renderer/Renderer.h"
namespace KG3D_Destruction{
    DestructionMesh::DestructionMesh(){
		indices = nullptr;
		blast_mesh = nullptr;
		is_root = false;
		chunk_depth = -1;
		name = "";
		mesh_transform = new Transform();
    }

    DestructionMesh::~DestructionMesh(){
		DESTRUCTION_DELETE_ARRAY(indices);
		DESTRUCTION_DELETE_ARRAY(vertices);
		DESTRUCTION_DELETE_ARRAY(texture_coords);
		DESTRUCTION_DELETE_ARRAY(normals);
		DESTRUCTION_DELETE_ARRAY(tangents);
		DESTRUCTION_DELETE(mesh_transform);
		DESTRUCTION_DELETE(blast_mesh);
		materials.clear();
		m_pDestructionMesh.clear();
		blast_mesh->release();
    }

    void DestructionMesh::CreateVerticesFromData(){
		CD3D11_BUFFER_DESC bufferDesc(0, D3D11_BIND_VERTEX_BUFFER);
		D3D11_SUBRESOURCE_DATA initData{ nullptr, 0, 0 };
		bufferDesc.ByteWidth = sizeof(NvcVec3)*m_pMeshData.m_VertexCount;
		initData.pSysMem = vertices;
		BoundingBox::CreateFromPoints(m_pMeshData.m_BoundingBox, m_pMeshData.m_VertexCount,
			(const XMFLOAT3*)vertices, sizeof(XMFLOAT3));
		Graphics::Renderer::Get()->CreateBuffer(&bufferDesc,&initData,m_pMeshData.m_pVertices.GetAddressOf());
		
		bufferDesc.ByteWidth = sizeof(NvcVec3) * m_pMeshData.m_VertexCount;
		initData.pSysMem = normals;
		Graphics::Renderer::Get()->CreateBuffer(&bufferDesc, &initData, m_pMeshData.m_pNormals.GetAddressOf());

		bufferDesc.ByteWidth = sizeof(NvcVec2) * m_pMeshData.m_VertexCount;
		initData.pSysMem = texture_coords;
		m_pMeshData.m_pTexcoordArrays.resize(1);
		Graphics::Renderer::Get()->CreateBuffer(&bufferDesc, &initData, m_pMeshData.m_pTexcoordArrays[0].GetAddressOf());

		bufferDesc = CD3D11_BUFFER_DESC(m_pMeshData.m_IndexCount * sizeof(UINT), D3D11_BIND_INDEX_BUFFER);
		initData.pSysMem = indices;
		Graphics::Renderer::Get()->CreateBuffer(&bufferDesc, &initData, m_pMeshData.m_pIndices.GetAddressOf());
    }

    void DestructionMesh::SetExplosionDisplacement(FLOAT displacement){
		XMFLOAT3 new_pos = XMFLOAT3(base_displacement.x * displacement, base_displacement.y * displacement, base_displacement.z * displacement);
		mesh_transform->SetPosition(new_pos);
    }
    
}