<!--
 * @Author       : HGL
 * @Date         : 2022-06-14 10:12:15
 * @LastEditTime : 2022-07-08 16:50:21
 * @Description  : 
 * @FilePath     : \Destrution\README.md
-->
# Destrution
破坏系统编辑器

## Description
基于nvidia blast的破坏编辑器

## 3rdParty
1. Dx11
2. Assimp
3. Blast
4. PhysX 4.1
5. ImGui

## Editor
![Editor](/Image/editor.png "Editor")
![PhysX](/Image/physx.png "PhysX")

## 运行视频
![Destruction](/Image/demo.gif "Destruction")