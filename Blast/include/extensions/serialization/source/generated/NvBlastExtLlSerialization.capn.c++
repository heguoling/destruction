// Generated by Cap'n Proto compiler, DO NOT EDIT
// source: NvBlastExtLlSerialization.capn

#include "NvBlastExtLlSerialization.capn.h"

namespace capnp {
namespace schemas {
static const ::capnp::_::AlignedData<210> b_ce4f8468c36f427d = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
    125,  66, 111, 195, 104, 132,  79, 206,
     31,   0,   0,   0,   1,   0,   2,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      7,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0,  42,   1,   0,   0,
     37,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     33,   0,   0,   0, 111,   2,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  65,
    115, 115, 101, 116,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
     44,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     37,   1,   0,   0,  58,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     32,   1,   0,   0,   3,   0,   1,   0,
     44,   1,   0,   0,   2,   0,   1,   0,
      1,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   1,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     41,   1,   0,   0,  26,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     36,   1,   0,   0,   3,   0,   1,   0,
     48,   1,   0,   0,   2,   0,   1,   0,
      2,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   2,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     45,   1,   0,   0,  90,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     44,   1,   0,   0,   3,   0,   1,   0,
     56,   1,   0,   0,   2,   0,   1,   0,
      3,   0,   0,   0,   2,   0,   0,   0,
      0,   0,   1,   0,   3,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     53,   1,   0,   0,  50,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     48,   1,   0,   0,   3,   0,   1,   0,
     60,   1,   0,   0,   2,   0,   1,   0,
      4,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   4,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     57,   1,   0,   0, 122,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     56,   1,   0,   0,   3,   0,   1,   0,
     68,   1,   0,   0,   2,   0,   1,   0,
      5,   0,   0,   0,   2,   0,   0,   0,
      0,   0,   1,   0,   5,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     65,   1,   0,   0, 210,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     72,   1,   0,   0,   3,   0,   1,   0,
     84,   1,   0,   0,   2,   0,   1,   0,
      6,   0,   0,   0,   3,   0,   0,   0,
      0,   0,   1,   0,   6,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     81,   1,   0,   0,  82,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     80,   1,   0,   0,   3,   0,   1,   0,
     92,   1,   0,   0,   2,   0,   1,   0,
      7,   0,   0,   0,   3,   0,   0,   0,
      0,   0,   1,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     89,   1,   0,   0,  58,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     84,   1,   0,   0,   3,   0,   1,   0,
    112,   1,   0,   0,   2,   0,   1,   0,
      8,   0,   0,   0,   4,   0,   0,   0,
      0,   0,   1,   0,   8,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    109,   1,   0,   0,  50,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    104,   1,   0,   0,   3,   0,   1,   0,
    132,   1,   0,   0,   2,   0,   1,   0,
      9,   0,   0,   0,   5,   0,   0,   0,
      0,   0,   1,   0,   9,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    129,   1,   0,   0, 186,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    132,   1,   0,   0,   3,   0,   1,   0,
    160,   1,   0,   0,   2,   0,   1,   0,
     10,   0,   0,   0,   6,   0,   0,   0,
      0,   0,   1,   0,  10,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    157,   1,   0,   0, 162,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    160,   1,   0,   0,   3,   0,   1,   0,
    188,   1,   0,   0,   2,   0,   1,   0,
    104, 101,  97, 100, 101, 114,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
     13,  53, 177,  49, 251, 169, 225, 213,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    105,  68,   0,   0,   0,   0,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
     58, 191,  25, 204,  53,   8, 208, 191,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 104, 117, 110, 107,  67, 111, 117,
    110, 116,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    103, 114,  97, 112, 104,   0,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
     85,  58, 203, 170, 252, 203,  24, 240,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    108, 101,  97, 102,  67, 104, 117, 110,
    107,  67, 111, 117, 110, 116,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    102, 105, 114, 115, 116,  83, 117,  98,
    115, 117, 112, 112, 111, 114, 116,  67,
    104, 117, 110, 107,  73, 110, 100, 101,
    120,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     98, 111, 110, 100,  67, 111, 117, 110,
    116,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 104, 117, 110, 107, 115,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
    186,  26, 123,  74, 102, 140, 129, 146,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     98, 111, 110, 100, 115,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
     16,   0,   0,   0,   0,   0,   0,   0,
    103, 218, 234, 149,  60, 164,  61, 196,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    115, 117,  98, 116, 114, 101, 101,  76,
    101,  97, 102,  67, 104, 117, 110, 107,
     67, 111, 117, 110, 116, 115,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 104, 117, 110, 107,  84, 111,  71,
    114,  97, 112, 104,  78, 111, 100, 101,
     77,  97, 112,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_ce4f8468c36f427d = b_ce4f8468c36f427d.words;
#if !CAPNP_LITE
static const ::capnp::_::RawSchema* const d_ce4f8468c36f427d[] = {
  &s_92818c664a7b1aba,
  &s_bfd00835cc19bf3a,
  &s_c43da43c95eada67,
  &s_d5e1a9fb31b1350d,
  &s_f018cbfcaacb3a55,
};
static const uint16_t m_ce4f8468c36f427d[] = {6, 8, 2, 10, 7, 5, 3, 0, 1, 4, 9};
static const uint16_t i_ce4f8468c36f427d[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
const ::capnp::_::RawSchema s_ce4f8468c36f427d = {
  0xce4f8468c36f427d, b_ce4f8468c36f427d.words, 210, d_ce4f8468c36f427d, m_ce4f8468c36f427d,
  5, 11, i_ce4f8468c36f427d, nullptr, nullptr, { &s_ce4f8468c36f427d, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
static const ::capnp::_::AlignedData<70> b_d5e1a9fb31b1350d = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
     13,  53, 177,  49, 251, 169, 225, 213,
     31,   0,   0,   0,   1,   0,   2,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      0,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0, 130,   1,   0,   0,
     41,   0,   0,   0,  23,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     49,   0,   0,   0, 175,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  78,
    118,  66, 108,  97, 115, 116,  68,  97,
    116,  97,  66, 108, 111,  99, 107,   0,
      4,   0,   0,   0,   1,   0,   1,   0,
     65, 240,   6, 134,  96, 189, 146, 178,
      1,   0,   0,   0,  42,   0,   0,   0,
     84, 121, 112, 101,   0,   0,   0,   0,
     12,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     69,   0,   0,   0,  74,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     68,   0,   0,   0,   3,   0,   1,   0,
     80,   0,   0,   0,   2,   0,   1,   0,
      1,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   1,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     77,   0,   0,   0, 114,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     76,   0,   0,   0,   3,   0,   1,   0,
     88,   0,   0,   0,   2,   0,   1,   0,
      2,   0,   0,   0,   2,   0,   0,   0,
      0,   0,   1,   0,   2,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     85,   0,   0,   0,  42,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     80,   0,   0,   0,   3,   0,   1,   0,
     92,   0,   0,   0,   2,   0,   1,   0,
    100,  97, 116,  97,  84, 121, 112, 101,
      0,   0,   0,   0,   0,   0,   0,   0,
     15,   0,   0,   0,   0,   0,   0,   0,
     65, 240,   6, 134,  96, 189, 146, 178,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     15,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    102, 111, 114, 109,  97, 116,  86, 101,
    114, 115, 105, 111, 110,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    115, 105, 122, 101,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_d5e1a9fb31b1350d = b_d5e1a9fb31b1350d.words;
#if !CAPNP_LITE
static const ::capnp::_::RawSchema* const d_d5e1a9fb31b1350d[] = {
  &s_b292bd608606f041,
};
static const uint16_t m_d5e1a9fb31b1350d[] = {0, 1, 2};
static const uint16_t i_d5e1a9fb31b1350d[] = {0, 1, 2};
const ::capnp::_::RawSchema s_d5e1a9fb31b1350d = {
  0xd5e1a9fb31b1350d, b_d5e1a9fb31b1350d.words, 70, d_d5e1a9fb31b1350d, m_d5e1a9fb31b1350d,
  1, 3, i_d5e1a9fb31b1350d, nullptr, nullptr, { &s_d5e1a9fb31b1350d, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
static const ::capnp::_::AlignedData<32> b_b292bd608606f041 = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
     65, 240,   6, 134,  96, 189, 146, 178,
     48,   0,   0,   0,   2,   0,   0,   0,
     13,  53, 177,  49, 251, 169, 225, 213,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0, 170,   1,   0,   0,
     45,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     41,   0,   0,   0,  55,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  78,
    118,  66, 108,  97, 115, 116,  68,  97,
    116,  97,  66, 108, 111,  99, 107,  46,
     84, 121, 112, 101,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
      8,   0,   0,   0,   1,   0,   2,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     17,   0,   0,   0, 122,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      1,   0,   0,   0,   0,   0,   0,   0,
     13,   0,   0,   0, 146,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97, 115, 115, 101, 116,  68,  97, 116,
     97,  66, 108, 111,  99, 107,   0,   0,
    105, 110, 115, 116,  97, 110,  99, 101,
     68,  97, 116,  97,  66, 108, 111,  99,
    107,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_b292bd608606f041 = b_b292bd608606f041.words;
#if !CAPNP_LITE
static const uint16_t m_b292bd608606f041[] = {0, 1};
const ::capnp::_::RawSchema s_b292bd608606f041 = {
  0xb292bd608606f041, b_b292bd608606f041.words, 32, nullptr, m_b292bd608606f041,
  0, 2, nullptr, nullptr, nullptr, { &s_b292bd608606f041, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
CAPNP_DEFINE_ENUM(Type_b292bd608606f041, b292bd608606f041);
static const ::capnp::_::AlignedData<120> b_92818c664a7b1aba = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
    186,  26, 123,  74, 102, 140, 129, 146,
     31,   0,   0,   0,   1,   0,   3,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      1,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0,  98,   1,   0,   0,
     41,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     37,   0,   0,   0,  87,   1,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  78,
    118,  66, 108,  97, 115, 116,  67, 104,
    117, 110, 107,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
     24,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    153,   0,   0,   0,  74,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    152,   0,   0,   0,   3,   0,   1,   0,
    180,   0,   0,   0,   2,   0,   1,   0,
      1,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   1,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    177,   0,   0,   0,  58,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    172,   0,   0,   0,   3,   0,   1,   0,
    184,   0,   0,   0,   2,   0,   1,   0,
      2,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   2,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    181,   0,   0,   0, 138,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    184,   0,   0,   0,   3,   0,   1,   0,
    196,   0,   0,   0,   2,   0,   1,   0,
      3,   0,   0,   0,   2,   0,   0,   0,
      0,   0,   1,   0,   3,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    193,   0,   0,   0, 130,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    192,   0,   0,   0,   3,   0,   1,   0,
    204,   0,   0,   0,   2,   0,   1,   0,
      4,   0,   0,   0,   3,   0,   0,   0,
      0,   0,   1,   0,   4,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    201,   0,   0,   0, 122,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    200,   0,   0,   0,   3,   0,   1,   0,
    212,   0,   0,   0,   2,   0,   1,   0,
      5,   0,   0,   0,   4,   0,   0,   0,
      0,   0,   1,   0,   5,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    209,   0,   0,   0,  74,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    208,   0,   0,   0,   3,   0,   1,   0,
    220,   0,   0,   0,   2,   0,   1,   0,
     99, 101, 110, 116, 114, 111, 105, 100,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    118, 111, 108, 117, 109, 101,   0,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    112,  97, 114, 101, 110, 116,  67, 104,
    117, 110, 107,  73, 110, 100, 101, 120,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    102, 105, 114, 115, 116,  67, 104, 105,
    108, 100,  73, 110, 100, 101, 120,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 104, 105, 108, 100,  73, 110, 100,
    101, 120,  83, 116, 111, 112,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    117, 115, 101, 114,  68,  97, 116,  97,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_92818c664a7b1aba = b_92818c664a7b1aba.words;
#if !CAPNP_LITE
static const uint16_t m_92818c664a7b1aba[] = {0, 4, 3, 2, 5, 1};
static const uint16_t i_92818c664a7b1aba[] = {0, 1, 2, 3, 4, 5};
const ::capnp::_::RawSchema s_92818c664a7b1aba = {
  0x92818c664a7b1aba, b_92818c664a7b1aba.words, 120, nullptr, m_92818c664a7b1aba,
  0, 6, i_92818c664a7b1aba, nullptr, nullptr, { &s_92818c664a7b1aba, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
static const ::capnp::_::AlignedData<90> b_c43da43c95eada67 = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
    103, 218, 234, 149,  60, 164,  61, 196,
     31,   0,   0,   0,   1,   0,   1,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      2,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0,  90,   1,   0,   0,
     41,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     37,   0,   0,   0, 231,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  78,
    118,  66, 108,  97, 115, 116,  66, 111,
    110, 100,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
     16,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97,   0,   0,   0,  58,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     92,   0,   0,   0,   3,   0,   1,   0,
    120,   0,   0,   0,   2,   0,   1,   0,
      1,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   1,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    117,   0,   0,   0,  42,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    112,   0,   0,   0,   3,   0,   1,   0,
    124,   0,   0,   0,   2,   0,   1,   0,
      2,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   2,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    121,   0,   0,   0,  74,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    120,   0,   0,   0,   3,   0,   1,   0,
    148,   0,   0,   0,   2,   0,   1,   0,
      3,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   3,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    145,   0,   0,   0,  74,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    144,   0,   0,   0,   3,   0,   1,   0,
    156,   0,   0,   0,   2,   0,   1,   0,
    110, 111, 114, 109,  97, 108,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97, 114, 101,  97,   0,   0,   0,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 101, 110, 116, 114, 111, 105, 100,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
     10,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    117, 115, 101, 114,  68,  97, 116,  97,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_c43da43c95eada67 = b_c43da43c95eada67.words;
#if !CAPNP_LITE
static const uint16_t m_c43da43c95eada67[] = {1, 2, 0, 3};
static const uint16_t i_c43da43c95eada67[] = {0, 1, 2, 3};
const ::capnp::_::RawSchema s_c43da43c95eada67 = {
  0xc43da43c95eada67, b_c43da43c95eada67.words, 90, nullptr, m_c43da43c95eada67,
  0, 4, i_c43da43c95eada67, nullptr, nullptr, { &s_c43da43c95eada67, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
static const ::capnp::_::AlignedData<120> b_f018cbfcaacb3a55 = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
     85,  58, 203, 170, 252, 203,  24, 240,
     31,   0,   0,   0,   1,   0,   1,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      4,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0, 154,   1,   0,   0,
     45,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     41,   0,   0,   0,  31,   1,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  78,
    118,  66, 108,  97, 115, 116,  83, 117,
    112, 112, 111, 114, 116,  71, 114,  97,
    112, 104,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
     20,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    125,   0,   0,   0,  82,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    124,   0,   0,   0,   3,   0,   1,   0,
    136,   0,   0,   0,   2,   0,   1,   0,
      1,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   1,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    133,   0,   0,   0, 106,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    132,   0,   0,   0,   3,   0,   1,   0,
    160,   0,   0,   0,   2,   0,   1,   0,
      2,   0,   0,   0,   1,   0,   0,   0,
      0,   0,   1,   0,   2,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    157,   0,   0,   0, 154,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    160,   0,   0,   0,   3,   0,   1,   0,
    188,   0,   0,   0,   2,   0,   1,   0,
      3,   0,   0,   0,   2,   0,   0,   0,
      0,   0,   1,   0,   3,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    185,   0,   0,   0, 162,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    188,   0,   0,   0,   3,   0,   1,   0,
    216,   0,   0,   0,   2,   0,   1,   0,
      4,   0,   0,   0,   3,   0,   0,   0,
      0,   0,   1,   0,   4,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    213,   0,   0,   0, 162,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
    216,   0,   0,   0,   3,   0,   1,   0,
    244,   0,   0,   0,   2,   0,   1,   0,
    110, 111, 100, 101,  67, 111, 117, 110,
    116,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     99, 104, 117, 110, 107,  73, 110, 100,
    105,  99, 101, 115,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97, 100, 106,  97,  99, 101, 110,  99,
    121,  80,  97, 114, 116, 105, 116, 105,
    111, 110,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97, 100, 106,  97,  99, 101, 110, 116,
     78, 111, 100, 101,  73, 110, 100, 105,
     99, 101, 115,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     97, 100, 106,  97,  99, 101, 110, 116,
     66, 111, 110, 100,  73, 110, 100, 105,
     99, 101, 115,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   3,   0,   1,   0,
      8,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     14,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_f018cbfcaacb3a55 = b_f018cbfcaacb3a55.words;
#if !CAPNP_LITE
static const uint16_t m_f018cbfcaacb3a55[] = {2, 4, 3, 1, 0};
static const uint16_t i_f018cbfcaacb3a55[] = {0, 1, 2, 3, 4};
const ::capnp::_::RawSchema s_f018cbfcaacb3a55 = {
  0xf018cbfcaacb3a55, b_f018cbfcaacb3a55.words, 120, nullptr, m_f018cbfcaacb3a55,
  0, 5, i_f018cbfcaacb3a55, nullptr, nullptr, { &s_f018cbfcaacb3a55, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
static const ::capnp::_::AlignedData<34> b_bfd00835cc19bf3a = {
  {   0,   0,   0,   0,   5,   0,   6,   0,
     58, 191,  25, 204,  53,   8, 208, 191,
     31,   0,   0,   0,   1,   0,   0,   0,
    224, 117, 131, 195, 250,  88,  74, 154,
      1,   0,   7,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     21,   0,   0,   0,  34,   1,   0,   0,
     37,   0,   0,   0,   7,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     33,   0,   0,   0,  63,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     78, 118,  66, 108,  97, 115, 116,  69,
    120, 116,  76, 108,  83, 101, 114, 105,
     97, 108, 105, 122,  97, 116, 105, 111,
    110,  46,  99,  97, 112, 110,  58,  85,
     85,  73,  68,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   1,   0,   1,   0,
      4,   0,   0,   0,   3,   0,   4,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   1,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     13,   0,   0,   0,  50,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      8,   0,   0,   0,   3,   0,   1,   0,
     20,   0,   0,   0,   2,   0,   1,   0,
    118,  97, 108, 117, 101,   0,   0,   0,
     13,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
     13,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0,
      0,   0,   0,   0,   0,   0,   0,   0, }
};
::capnp::word const* const bp_bfd00835cc19bf3a = b_bfd00835cc19bf3a.words;
#if !CAPNP_LITE
static const uint16_t m_bfd00835cc19bf3a[] = {0};
static const uint16_t i_bfd00835cc19bf3a[] = {0};
const ::capnp::_::RawSchema s_bfd00835cc19bf3a = {
  0xbfd00835cc19bf3a, b_bfd00835cc19bf3a.words, 34, nullptr, m_bfd00835cc19bf3a,
  0, 1, i_bfd00835cc19bf3a, nullptr, nullptr, { &s_bfd00835cc19bf3a, nullptr, nullptr, 0, 0, nullptr }
};
#endif  // !CAPNP_LITE
}  // namespace schemas
}  // namespace capnp

// =======================================================================================

namespace Nv {
namespace Blast {
namespace Serialization {

// Asset
constexpr uint16_t Asset::_capnpPrivate::dataWordSize;
constexpr uint16_t Asset::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind Asset::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* Asset::_capnpPrivate::schema;
#endif  // !CAPNP_LITE

// NvBlastDataBlock
constexpr uint16_t NvBlastDataBlock::_capnpPrivate::dataWordSize;
constexpr uint16_t NvBlastDataBlock::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind NvBlastDataBlock::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* NvBlastDataBlock::_capnpPrivate::schema;
#endif  // !CAPNP_LITE

// NvBlastChunk
constexpr uint16_t NvBlastChunk::_capnpPrivate::dataWordSize;
constexpr uint16_t NvBlastChunk::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind NvBlastChunk::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* NvBlastChunk::_capnpPrivate::schema;
#endif  // !CAPNP_LITE

// NvBlastBond
constexpr uint16_t NvBlastBond::_capnpPrivate::dataWordSize;
constexpr uint16_t NvBlastBond::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind NvBlastBond::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* NvBlastBond::_capnpPrivate::schema;
#endif  // !CAPNP_LITE

// NvBlastSupportGraph
constexpr uint16_t NvBlastSupportGraph::_capnpPrivate::dataWordSize;
constexpr uint16_t NvBlastSupportGraph::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind NvBlastSupportGraph::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* NvBlastSupportGraph::_capnpPrivate::schema;
#endif  // !CAPNP_LITE

// UUID
constexpr uint16_t UUID::_capnpPrivate::dataWordSize;
constexpr uint16_t UUID::_capnpPrivate::pointerCount;
#if !CAPNP_LITE
constexpr ::capnp::Kind UUID::_capnpPrivate::kind;
constexpr ::capnp::_::RawSchema const* UUID::_capnpPrivate::schema;
#endif  // !CAPNP_LITE


}  // namespace
}  // namespace
}  // namespace

