<!--
 * @Author       : HGL
 * @Date         : 2022-06-21 14:02:55
 * @LastEditTime : 2022-07-08 16:54:22
 * @Description  : 
 * @FilePath     : \Destrution\CHANGELOG.md
-->
# 更新日志
##  2022-07-08
### 新增
* MaterialManager对材质资源进行管理
* BlastManager类updatemesh函数实现
### 优化
* BlasterManager updatemesh时mesh的绘制
* chunktree显示chunk的遍历
* MeshData结构调整
##  2022-07-07
### 新增
* BlastManager类通过fracture tool创建blast mesh。
* 通过ui选中mesh并进行切割
##  2022-07-06
### 新增
* 新增physics module处理物理场景创建以及管理
* BlastManager类updatemesh函数实现
### 优化
* Blaster模块重构，blaster直接由scene、manager组成
* 选中物体时，先选中actor，再选中对应的mesh
* 内存泄露优化
* 通过renderer调用d3d11device方式调整
### 删除
* 删除Blast创建物理资源部分
* 删除blastActor中的BlastAsset成员以及参与的成员函数
##  2022-07-05
### 优化
* 更新PhysX API版本至4.1
##  2022-07-04
### 新增
* 包围盒绘制
* BlastErrorCallback
##  2022-06-30
### 优化
* renderer渲染方式，弃用模型加载，每次加载单个mesh，便于以后绘制每个chunk的mesh
* 增加渲染队列，由renderer进行管理
##  2022-06-29
### 新增
* physx场景加载
* piker拾取器
* mesh包围盒计算
* blaster updateMesh
* mesh Importer初始化Nv::Blast::Mesh
##  2022-06-28
### 新增
* BlastScene渲染
* chunk tree生成、显示 
### 优化
* 改用modelManager完成模型导入及管理
* Logger作为全局变量，不需要通过application全局打印log
##  2022-06-23
### 新增
* BlastMesh导入
* BlastActor渲染
### 优化
* 改用modelManager完成模型导入及管理
* Logger可变参数打印
### 删除
* ModelManager
* TextureManager
* MathUtils
##  2022-06-22
### 新增
* UIDrawer添加菜单栏
* Blaster类，作为编辑器的破碎模块
* renderer采用cascadeShadow实时阴影
### 优化
* 改用modelManager完成模型导入及管理
### 删除
* basiceEffect
* objLoader
* GameObject
##  2022-06-21
### 新增
* Logger模块，可以在Application中输出对应的调试信息
* BlastAsset类。可直接使用的资产数据
### 优化
* 模块间的互相调用，取消模块之间的绑定，通过Appliacation调用
### 删除
* LogUtils